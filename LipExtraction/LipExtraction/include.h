#pragma once

#include "library.h"

#include <vector>
#include <string>
#include <sstream>
#include <future>
#include <thread>
#include <chrono>
#include <cmath>
#include <list>
#include <iostream>
#include <mutex>
#include <algorithm>
#include <iomanip>
#include <memory>
#include <fstream>


using namespace std;

template<typename Error = runtime_error>
inline void Assert(bool expr, const string &info = string{}) {
	if (!expr) {
		throw Error{ info };
	}
}