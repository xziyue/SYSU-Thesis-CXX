#pragma once

#ifdef NDEBUG
#pragma comment(lib, "dlib_release_64bit_msvc1912.lib")
#pragma comment(lib, "opencv_world341.lib")
#else
#pragma comment(lib, "dlib_debug_64bit_msvc1912.lib")
#pragma comment(lib, "opencv_world341d.lib")
#endif
