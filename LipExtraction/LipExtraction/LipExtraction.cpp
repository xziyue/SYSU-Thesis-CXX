#include "helper.h"
#include <cstdlib>

int main(int argc, char **argv)
{
	if (argc != 3) {
		cout << "invalid number of parameters\n";
		cout << "usage: LipExtraction videoFilename outputFilename\n";
		return 0;
	}

	string videoPath{argv[1]};
	string outputPath{argv[2]};

	cout << "Video filename: \n " << videoPath << "\n";
	cout << "Output filename: \n" << outputPath << "\n";
	cout << "Proceed?(Y/N)\n";

	char key;
	cin >> key;
	if (!(key == 'y' || key == 'Y')) {
		return 0;
	}

	auto lipFeature = move(ExtractVideoLipFeature(videoPath));

	SaveLipFeature(lipFeature, outputPath);

	cout << "\nLip feature saved to " << outputPath << "\n";


    return 0;
}

