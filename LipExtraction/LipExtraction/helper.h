#pragma once

#include "include.h"
#include "dlib/image_processing/frontal_face_detector.h"
#include "dlib/image_processing/render_face_detections.h"
#include "dlib/image_processing.h"
#include "dlib/gui_widgets.h"
#include "dlib/image_io.h"
#include "dlib/opencv.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using Int2 = pair<int, int>;

using Double2 = pair<double, double>;

constexpr const int resampleRuns = 3;

vector<Int2> AssignTask(int numOfTask, int numOfPart);

const string predictorDataPath{R"(C:\Users\xziyu\OneDrive\Thesis\Python\shape_predictor_68_face_landmarks.dat)"};


struct VideoInfo {
	string videoFilename;
	int videoFPS{ 0 };
	double videoFrameTime{ 0.0 };
	int videoNumOfFrame{ 0 };
};

struct VideoLipFeature {
	VideoInfo videoInfo;
	list<vector<Double2>> frameList;
};

VideoInfo GetVideoInfo(const string &filename);

void ExtractTask(int threadID, Int2 myTask, const VideoInfo &videoInfo,
	const dlib::shape_predictor &predictor,
	vector<unique_ptr<atomic<int>>> &progressList, 
	vector<list<vector<Double2>>> &resultList,
	mutex &resultListLock, mutex &coutLock);

VideoLipFeature ExtractVideoLipFeature(const string & filename,
	chrono::milliseconds outputInterval = chrono::milliseconds{ 5000 });

void SaveLipFeature(const VideoLipFeature &lipFeature, const string &filename);