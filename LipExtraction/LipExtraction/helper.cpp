#include "helper.h"

vector<Int2> AssignTask(int numOfTask, int numOfPart) {
	Assert(numOfTask > 0);
	Assert(numOfPart > 0);
	Assert(numOfTask >= numOfPart);

	vector<Int2> result;
	result.resize(numOfPart);

	auto stepSize = numOfTask / numOfPart;

	for (auto i = 0; i < numOfPart - 1; ++i) {
		result[i] = Int2{i * stepSize, (i+1) * stepSize};
	}

	result[numOfPart - 1] = Int2{ (numOfPart - 1) * stepSize, numOfTask };

	return result;
}

VideoInfo GetVideoInfo(const string & filename)
{
	VideoInfo result;

	cv::VideoCapture cap{ filename };

	Assert(cap.isOpened(), string{ "unable to open file " } +filename);

	auto fps_f = cap.get(cv::CAP_PROP_FPS);
	auto fps = (int)round(fps_f);

	Assert(abs((double)fps - fps_f) < 1.0e-3, "video format not supported: invalid fps");
	
	auto numOfFrame_f = cap.get(cv::CAP_PROP_FRAME_COUNT);
	auto numOfFrame = (int)round(numOfFrame_f);

	Assert(abs((double)numOfFrame - numOfFrame_f) < 1.0e-3, "video format not supported: invalid number of frames");

	result.videoFilename = filename;
	result.videoFPS = fps;
	result.videoNumOfFrame = numOfFrame;
	result.videoFrameTime = 1.0 / fps_f;

	return result;
}

void ExtractTask(int threadID, Int2 myTask, const VideoInfo & videoInfo,
	const dlib::shape_predictor &predictor,
	vector<unique_ptr<atomic<int>>> &progressList,
	vector<list<vector<Double2>>>& resultList,
	mutex & resultListLock, mutex &coutLock)
{
	cv::VideoCapture cap{ videoInfo.videoFilename };

	auto detector = dlib::get_frontal_face_detector();

	Assert(cap.isOpened(), string{ "unable to open file " } + videoInfo.videoFilename);

	cap.set(cv::CAP_PROP_POS_FRAMES, myTask.first);

	auto taskCount = myTask.second - myTask.first;

	list<vector<Double2>> myLipFeature;

	for (auto i = 0; i < taskCount; ++i) {
		//cout << i << endl;
		cv::Mat rawFrame;
		//cv::Mat frame;
		if (!cap.read(rawFrame)) {
			stringstream sstr;
			sstr << "unable to read frame " << myTask.first + i
				<< "/" << videoInfo.videoNumOfFrame;
			throw runtime_error{ sstr.str() };
		}

		//cvtColor(rawFrame, frame, CV_BGR2GRAY);
		//cv::equalizeHist(frame, frame);

		dlib::cv_image<dlib::bgr_pixel> cvImageRaw(rawFrame);
		//dlib::cv_image<unsigned char> cvImage(frame);

		vector<dlib::rectangle> faces{ detector(cvImageRaw) };
		
		vector<Double2> thisLipFeature;

		if (faces.empty()) {
			stringstream sstr;
			sstr << "unable to detect face in frame " << myTask.first + i
				<< "/" << videoInfo.videoNumOfFrame;
			coutLock.lock();
			cout << sstr.str() << "\n";
			coutLock.unlock();
		}
		else {
			if (faces.size() > 1) {
				stringstream sstr;
				sstr << "more than one faces in frame " << myTask.first + i
					<< "/" << videoInfo.videoNumOfFrame;
				coutLock.lock();
				cout << sstr.str() << "\n";
				coutLock.unlock();
			}
			
			// extract lip features
			thisLipFeature.resize(20, Double2{ 0.0,0.0 });

			/*
			for (auto j = 0; j < resampleRuns; ++j) {
				dlib::full_object_detection shape{ predictor(cvImage, faces[0]) };
				for (auto k = 48; k < 68; ++k) {
					const auto &dlibPoint = shape.part(k);
					thisLipFeature[k - 48].first += (double)dlibPoint.x();
					thisLipFeature[k - 48].second += (double)dlibPoint.y();
				}
			}
			*/

			for (auto j = 0; j < resampleRuns; ++j) {
				dlib::full_object_detection shape{ predictor(cvImageRaw, faces[0]) };
				for (auto k = 48; k < 68; ++k) {
					const auto &dlibPoint = shape.part(k);
					thisLipFeature[k - 48].first += (double)dlibPoint.x();
					thisLipFeature[k - 48].second += (double)dlibPoint.y();
				}
			}

			for (auto j = 0; j < (int)thisLipFeature.size(); ++j) {
				thisLipFeature[j].first /= (double)resampleRuns;
				thisLipFeature[j].second /= (double)resampleRuns;
			}

		}

		myLipFeature.emplace_back(move(thisLipFeature));

		(*progressList[threadID])++;

	}

	resultListLock.lock();
	resultList[threadID] = move(myLipFeature);
	resultListLock.unlock();

	cap.release();
}

VideoLipFeature ExtractVideoLipFeature(const string & filename, chrono::milliseconds outputInterval)
{
	VideoLipFeature result;
	
	VideoInfo videoInfo{GetVideoInfo(filename)};

	result.videoInfo = videoInfo;

	auto taskCount = videoInfo.videoNumOfFrame;
	auto taskPart = max(1, (int)thread::hardware_concurrency()- 1);
	//auto taskPart = 1;

	auto taskAssignment = move(AssignTask(taskCount, taskPart));

	vector<unique_ptr<atomic<int>>> progressList(taskPart);
	vector<list<vector<Double2>>> resultList(taskPart);
	mutex resultListLock;
	mutex coutLock;

	auto predictor = dlib::shape_predictor{};
	dlib::deserialize(predictorDataPath) >> predictor;

	for (auto i = 0; i < taskPart; ++i) {
		progressList[i] = move(make_unique<atomic<int>>(0));
	}


	auto startTime = chrono::system_clock::now();

	for (auto i = 0; i < taskPart; ++i) {
		thread newThread{&ExtractTask, i, taskAssignment[i], ref(videoInfo), 
		ref(predictor), ref(progressList),  
		ref(resultList), ref(resultListLock), ref(coutLock)};
		newThread.detach();
	}

	auto lastOutputTime = startTime - 0.6 * outputInterval;
	while (true) {
		auto nowTime = chrono::system_clock::now();
		auto timeDifference = nowTime - startTime;

		bool allFinished = true;
		resultListLock.lock();
		for (auto i = 0; i < taskPart; ++i) {
			if (resultList[i].empty()) {
				allFinished = false;
				break;
			}
		}
		resultListLock.unlock();

		if (allFinished) {
			stringstream sstr;
			sstr << "\nLip feature extraction finished in "
				<< chrono::duration_cast<chrono::seconds>(timeDifference).count()
				<< "s\n";
			cout << sstr.str();

			// concatenate lip features
			for (auto i = 0; i < taskPart; ++i) {
				result.frameList.splice(result.frameList.end(), resultList[i]);
			}

			return result;
		}
		else {
			auto outputTimeDifference = nowTime - lastOutputTime;
			if (outputTimeDifference > outputInterval) {
				auto frameCount = 0;
				for (auto i = 0; i < taskPart; ++i) {
					frameCount += *progressList[i];
				}
				double progress = (double)frameCount * 100.0 / (double)videoInfo.videoNumOfFrame;
				double speed = (double)frameCount / (double)chrono::duration_cast<chrono::seconds>(timeDifference).count();
				double estimation = (double)(videoInfo.videoNumOfFrame - frameCount) / speed;

				stringstream sstr;
				sstr << frameCount << "/" << videoInfo.videoNumOfFrame << " ("
					<< fixed << setprecision(2) << progress << "%) | time used: "
					<< fixed << setprecision(0)
					<< chrono::duration_cast<chrono::seconds>(timeDifference).count() << "s |"
					<< " estimated time left: " << fixed << setprecision(0)
					<< round(estimation) << "s \n";
				coutLock.lock();
				cout << sstr.str();
				coutLock.unlock();

				lastOutputTime = nowTime;
			}
			else {
				this_thread::sleep_for(chrono::milliseconds{25});
			}

		}
	}
}


template<typename T, typename... Style>
void WritePack(fstream &outfile, const T &t) {
	outfile << t;
}

template<typename T, typename... Style>
void WritePack(fstream &outfile, const T &t, Style... style) {
	outfile << t;
	WritePack(outfile, style...);
}

void WritePack(fstream &outfile) {}

template<typename T, typename... Style>
void WriteProperty(fstream &outfile, const string &propertyName, const T &propertyValue, Style... style) {
	WritePack(outfile, forward<Style>(style)...);
	outfile << propertyName << "=" << propertyValue << "\n";
}

template<typename T, typename... Style>
void WritePoint(fstream &outfile, const T &point, Style... style) {
	WritePack(outfile, forward<Style>(style)...);
	outfile << point.first << ",";
	WritePack(outfile, forward<Style>(style)...);
	outfile << point.second;
}

void SaveLipFeature(const VideoLipFeature & lipFeature, const string & filename)
{
	Assert(lipFeature.videoInfo.videoNumOfFrame == lipFeature.frameList.size(), "invalid lip feature");

	fstream outfile{ filename, ios::out };
	outfile << "[Property]\n";
	WriteProperty(outfile, "videoFilename", lipFeature.videoInfo.videoFilename);
	WriteProperty(outfile, "videoFPS", lipFeature.videoInfo.videoFPS);
	WriteProperty(outfile, "videoFrameTime", lipFeature.videoInfo.videoFrameTime, fixed, setprecision(8));
	WriteProperty(outfile, "videoNumOfFrame", lipFeature.videoInfo.videoNumOfFrame);
	WriteProperty(outfile, "numOfPointPerFrame", 20);

	outfile << "[Frame]\n";

	auto iter = lipFeature.frameList.begin();
	// write frames
	for (auto i = 0; i < lipFeature.videoInfo.videoNumOfFrame; ++i, ++iter) {
		outfile << "frameID=" << i << " ";
		const vector<Double2> &points = *iter;
		for (auto j = 0; j < (int)points.size(); ++j) {
			WritePoint(outfile, points[j], fixed, setprecision(8));
			outfile << ";";
		}
		outfile << "\n";
	}

	outfile.close();
}
