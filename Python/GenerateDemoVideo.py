import cv2
from PIL import Image, ImageDraw, ImageFont
from SampleManagement import GetFrameCountLowerBound, CreateSampleStorage
from LipFeatureDemo import GetLipFeatureAnimation
from LipFeatureReader import ReadLipFeatureFromTxt
from LipFeatureProcessor import GetStandardizedVideoLipFeature
import codecs
import keras
from keras import *
from keras.layers import *
from keras.utils import *
from keras.callbacks import *
import numpy as np

writer = cv2.VideoWriter()
fourcc = cv2.VideoWriter_fourcc(*'DIVX')

videoReader = cv2.VideoCapture()
videoReader.open('../Sample/sample3-15fps.mp4')

templateImg = cv2.imread('../Demo/DemoTemplate.jpg')
height = templateImg.shape[0]
width = templateImg.shape[1]

fps = 15
timePerFrame = 1.0 / float(fps)

writer.open('../Demo/demo.avi', fourcc, float(fps), (width, height))

font = ImageFont.truetype('C:/windows/fonts/simhei.ttf', 55)

sampleFilename = '../Sample/sample3-15fps-lipFeature.txt'
sampleSubtitleFilename = '../Sample/sample3-subtitle.ass'

# load sample data
valStorage = CreateSampleStorage(sampleFilename , sampleSubtitleFilename)
X, Y = valStorage.GetKerasXY(30)

lipFeature = GetStandardizedVideoLipFeature(ReadLipFeatureFromTxt(sampleFilename))

videoWindowSize = (507, 380)
lipWindowSize = (380, 380)

correctR, correctG, correctB = 0, 255, 0
wrongR, wrongG, wrongB = 255, 0, 0

fontLeftCenter = (285, 655)
fontRightCenter = (762, 655)

modelFilename = '../Model/3_31_0130_model.keras'
modelWeightFilename = '../Model/3_31_0130_weights-epoch482-acc0.9576-val_acc0.8333.hdf5'

# load model
model = keras.models.load_model(modelFilename)
model.load_weights(modelWeightFilename)
resultArray = model.predict_classes(X)

for i in range(len(valStorage.trainingSampleX)):
    print('%d/%d'%(i + 1, len(valStorage.trainingSampleX)))

    frameLocation = valStorage.correspondingFrame[i]

    numOfFrame = frameLocation[1] - frameLocation[0]

    lipImgSeq = GetLipFeatureAnimation(lipFeature.frameList[frameLocation[0]:frameLocation[1]], lipWindowSize)

    videoReader.set(cv2.CAP_PROP_POS_FRAMES, frameLocation[0])
    videoImgSeq = []

    frameImage = []

    for j in range(numOfFrame):
        hasFrame, frame = videoReader.read()
        assert hasFrame
        newFrame = cv2.resize(frame, videoWindowSize)
        videoImgSeq.append(newFrame)


    for j in range(numOfFrame):
        img = np.copy(templateImg)

        for k in range(170, 170 + videoWindowSize[1]):
            for l in range(32, 32 + videoWindowSize[0]):
                img[k][l] = videoImgSeq[j][k - 170][l - 32]

        for k in range(170, 170 + lipWindowSize[1]):
            for l in range(554, 554 + lipWindowSize[0]):
                img[k][l] = lipImgSeq[j][k - 170][l - 554]

        frameImage.append(img)

    # write text
    for j in range(numOfFrame):
        pilImage = Image.fromarray(frameImage[j])
        draw = ImageDraw.Draw(pilImage)

        label = valStorage.labelVector[valStorage.trainingSampleY[i]]
        w,h = draw.textsize(label, font = font)
        leftPos = (fontLeftCenter[0] -  int(w/2), fontLeftCenter[1] - int(h/2))
        draw.text(leftPos, label, fill = (0,0,0), font = font)
        frameImage[j] = np.array(pilImage)

    prediction = resultArray[i]
    predictionLabel = valStorage.labelVector[prediction]
    for j in range(30):
        pilImage = Image.fromarray(frameImage[-1])
        draw = ImageDraw.Draw(pilImage)

        w, h = draw.textsize(predictionLabel, font=font)
        rightPos = (fontRightCenter[0] -  int(w/2), fontRightCenter[1] - int(h/2))
        if prediction == valStorage.trainingSampleY[i]:
            draw.text(rightPos, predictionLabel, fill = (correctB, correctG, correctR), font = font)
        else:
            draw.text(rightPos, predictionLabel, fill=(wrongB, wrongG, wrongR), font=font)
        frameImage.append(np.array(pilImage))


    for img in frameImage:
        writer.write(img)



writer.release()