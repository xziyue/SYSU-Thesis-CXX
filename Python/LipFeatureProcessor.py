from LipFeatureReader import VideoLipFeature, lipFeatureLength, ReadNeutralLipFeature
import cv2
import copy


import numpy as np

def TransformPoint(pointID):
    return pointID - 48

def GetWaivedPointLip(lip, sortedWaivedPoints):
    assert lipFeatureLength >= len(sortedWaivedPoints)

    newLipFeatureLength = lipFeatureLength - len(sortedWaivedPoints)

    newLipFeature = np.zeros((newLipFeatureLength, 2), np.float64)

    canceledPointIndex = 0
    lipPointIndex = 0

    for i in range(lipFeatureLength):
        if canceledPointIndex < len(sortedWaivedPoints):
            if i == sortedWaivedPoints[canceledPointIndex]:
                canceledPointIndex += 1
                continue

        newLipFeature[lipPointIndex] = lip[i]
        lipPointIndex += 1

    return newLipFeature

commonWaivedPoints =  [51, 62, 66, 57, 48, 54]

def GetStandardizedVideoLipFeature(videoLipFeature, waivedPoints = commonWaivedPoints):
    waivedPoints = copy.deepcopy(waivedPoints)
    waivedPoints.sort()

    for i in range(len(waivedPoints)):
        waivedPoints[i] = TransformPoint(waivedPoints[i])

    newLipFeatureLength = lipFeatureLength - len(waivedPoints)

    newVideoLipFeature = VideoLipFeature()
    newVideoLipFeature.videoInfo = videoLipFeature.videoInfo
    newVideoLipFeature.numOfPointPerFrame = newLipFeatureLength

    for lip in videoLipFeature.frameList:
        assert lip is not None
        assert len(lip) == lipFeatureLength
        newVideoLipFeature.frameList.append(GetWaivedPointLip(lip, waivedPoints))

    # translate near the origin
    for i in range(len(newVideoLipFeature.frameList)):
        xMin = np.min(newVideoLipFeature.frameList[i][:,0])
        yMin = np.min(newVideoLipFeature.frameList[i][:,1])
        minArray = np.array((xMin, yMin), np.float64)
        newVideoLipFeature.frameList[i][:] -= minArray

    globalXMax = 0.0
    globalYMax = 0.0

    # find global xMax and yMax
    for i in range(len(newVideoLipFeature.frameList)):
        xMax = np.max(newVideoLipFeature.frameList[i][:, 0])
        yMax = np.max(newVideoLipFeature.frameList[i][:, 1])
        if xMax > globalXMax:
            globalXMax = xMax
        if yMax > globalYMax:
            globalYMax = yMax


    scaleArray = np.array((globalXMax, globalYMax), np.float64)
    # standardize all points to (0,1) and translate lip to center
    for i in range(len(newVideoLipFeature.frameList)):
        newVideoLipFeature.frameList[i][:] /= scaleArray
        xMax = np.max(newVideoLipFeature.frameList[i][:, 0])
        yMax = np.max(newVideoLipFeature.frameList[i][:, 1])
        offset = np.array((xMax, yMax), np.float64)
        translation = (np.ones(2, np.float64) - offset) / 2.0
        newVideoLipFeature.frameList[i][:] += translation

    return newVideoLipFeature

def GetTruncatedLipFeature(videoLipFeature, waivedPoints = commonWaivedPoints):
    waivedPoints = copy.deepcopy(waivedPoints)
    waivedPoints.sort()

    for i in range(len(waivedPoints)):
        waivedPoints[i] = TransformPoint(waivedPoints[i])

    newLipFeatureLength = lipFeatureLength - len(waivedPoints)

    newVideoLipFeature = VideoLipFeature()
    newVideoLipFeature.videoInfo = videoLipFeature.videoInfo
    newVideoLipFeature.numOfPointPerFrame = newLipFeatureLength

    for lip in videoLipFeature.frameList:
        assert lip is not None
        assert len(lip) == lipFeatureLength
        newVideoLipFeature.frameList.append(GetWaivedPointLip(lip, waivedPoints))

    return newVideoLipFeature

