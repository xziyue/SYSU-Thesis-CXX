from SampleManagement import CreateBlankSampleStorage, CreateSampleStorage, GetRawLip
from AssReader import AssReader
import numpy as np
import copy
import matplotlib.pyplot as plt
import operator
import math
import random
import pickle

#timeStep = 5

#storage = CreateBlankSampleStorage('../Sample/sample3-15fps-lipFeature.txt','../Sample/sample3-subtitle.ass', timeStep)
#notBlankStorage = CreateSampleStorage('../Sample/sample3-15fps-lipFeature.txt','../Sample/sample3-subtitle.ass')
#featureLength = len(storage.trainingSampleX[0])

class ClusterInfo:
    left = -1
    length = -1
    type = -1


def MergeClusterInfo(left, right):
    assert left.left + left.length <= right.left
    assert left.type == right.type
    cluster = ClusterInfo()
    cluster.left = left.left
    cluster.length = right.left + right.length - left.left
    cluster.type = left.type
    return cluster

class SampleSegmentataion:
    timeStep = None
    blankSampleStorage = None
    notBlankSampleStorage = None
    notBlankRawLip = None
    segmentationSample = None
    lipFeatureLength = None
    blankMean = None
    eigenVector = []

    def __init__(self, blankLipTxt, blankLipAss, notBlankTxt, notBlankAss, timeStep):
        assert timeStep > 0
        self.timeStep = timeStep
        self.blankSampleStorage = CreateBlankSampleStorage(blankLipTxt, blankLipAss, timeStep)
        self.notBlankSampleStorage = CreateSampleStorage(notBlankTxt, notBlankAss)
        self.notBlankRawLip = GetRawLip(notBlankTxt)
        self.segmentationSample = []
        self.eigenVector = []
        self.lipFeatureLength = len(self.blankSampleStorage.trainingSampleX[0])

    def ReconstructLipFeature(self, _lipFeature):
        lipFeature = np.copy(_lipFeature)
        assert len(self.eigenVector) > 0
        assert len(lipFeature) == self.lipFeatureLength
        lipFeature -= self.blankMean
        ret = np.zeros(len(self.eigenVector), np.float64)
        for i in range(len(ret)):
            ret[i] = self.eigenVector[i].dot(lipFeature)
        return ret

    def CreateSegmentationSample(self, numOfEV, numOfSample, dropOutCoef = 0.0):
        assert 0 < numOfEV < self.lipFeatureLength
        assert 0 < numOfSample < len(self.blankSampleStorage.trainingSampleX)

        self.eigenVector.clear()
        self.segmentationSample.clear()

        truncatedSample = [None] * len(self.notBlankSampleStorage.trainingSampleX)

        # truncate not blank samples
        for i in range(len(self.notBlankSampleStorage.trainingSampleX)):
            truncatedSample[i] = self.notBlankSampleStorage.trainingSampleX[i][:self.timeStep]
            truncatedSample[i] = np.array(np.concatenate(truncatedSample[i]).flat)

        # apply pca to dataset
        mean = np.zeros(self.lipFeatureLength, np.float64)
        for i in range(len(self.blankSampleStorage.trainingSampleX)):
            mean += self.blankSampleStorage.trainingSampleX[i]
        mean /= float(len(self.blankSampleStorage.trainingSampleX))
        self.blankMean = mean

        # construct sample matrix
        dataMatrix = np.zeros((len(self.blankSampleStorage.trainingSampleX), self.lipFeatureLength), np.float64)
        for i in range(len(self.blankSampleStorage.trainingSampleX)):
            dataMatrix[i] = self.blankSampleStorage.trainingSampleX[i]
            dataMatrix[i] -= mean
            #length = np.linalg.norm(dataMatrix[i])
            #[i] /= length

        u, d, v = np.linalg.svd(dataMatrix, False, True)

        for i in range(numOfEV):
            self.eigenVector.append(v[:, i])

        errorList = []
        notBlankRecon = []
        blankRecon = []

        for i in range(len(self.blankSampleStorage.trainingSampleX)):
            blankRecon.append(self.ReconstructLipFeature(self.blankSampleStorage.trainingSampleX[i]))

        for i in range(len(self.notBlankSampleStorage.trainingSampleX)):
            notBlankRecon.append(self.ReconstructLipFeature(truncatedSample[i]))

        # dropout some blank sample
        dropOutSet = set()
        for i in range(len(blankRecon)):
            rnd = random.uniform(0.0, 1.0)
            if rnd < dropOutCoef:
                dropOutSet.add(i)

        tempRecon = []
        for i in range(len(blankRecon)):
            if i not in dropOutSet:
                tempRecon.append(blankRecon[i])
        blankRecon = tempRecon

        for i in range(len(blankRecon)):
            error = 0.0
            for j in range(len(self.notBlankSampleStorage.trainingSampleX)):
                error += np.linalg.norm(blankRecon[i] - notBlankRecon[j])
            errorList.append((i, error))

        errorList.sort(key = operator.itemgetter(1), reverse=True)

        if numOfSample > len(blankRecon):
            print('number of segmentation sample greater than number of total sample')
            print('using numOfSampe = %d instead'%len(blankRecon))

        numOfSample = min(numOfSample, len(blankRecon))

        for i in range(numOfSample):
            self.segmentationSample.append(blankRecon[errorList[i][0]])

    def CheckIfBlank(self,interval, frameTime, assReader):
        leftCheck = not assReader.GetSubtitle(float(interval[0]) * frameTime)[0]
        rightCheck = not assReader.GetSubtitle(float(interval[1]) * frameTime)[0]
        return leftCheck and rightCheck

    def GetAnalytics(self, rawLip, assReader):
        assert len(self.segmentationSample) > 0
        xSet_R = []
        ySet_R = []
        xSet_G = []
        ySet_G = []
        xSet = []
        ySet = []

        totalLength = len(rawLip.frameList) - self.timeStep
        assert totalLength > 0
        xRange = totalLength

        for i in range(xRange):
            left = i
            right = i + self.timeStep

            thisLipFeature = rawLip.frameList[left: right]
            lipFeatureVec = np.concatenate(thisLipFeature)
            lipFeatureVec = np.array(lipFeatureVec.flat)

            lipRecon = self.ReconstructLipFeature(lipFeatureVec)

            subError = []
            for sample in self.segmentationSample:
                subError.append(np.linalg.norm(lipRecon - sample))

            subError = np.array(subError, np.float64)
            # minError = np.min(subError)
            minError = np.mean(subError)
            # minError = np.var(subError)
            minError = math.log(minError)

            xSet.append(i)
            ySet.append(minError)

            if self.CheckIfBlank((left, right), rawLip.videoInfo.videoFrameTime, assReader):
                xSet_G.append(i)
                ySet_G.append(minError)
            else:
                xSet_R.append(i)
                ySet_R.append(minError)

        return xSet_R, ySet_R, xSet_G, ySet_G, xSet, ySet

    def GetSegmentIndex(self, rawLip):
        assert len(self.segmentationSample) > 0
        xSet = []
        ySet = []

        totalLength = len(rawLip.frameList) - self.timeStep
        assert totalLength > 0
        xRange = totalLength

        for i in range(xRange):
            left = i
            right = i + self.timeStep

            thisLipFeature = rawLip.frameList[left: right]
            lipFeatureVec = np.concatenate(thisLipFeature)
            lipFeatureVec = np.array(lipFeatureVec.flat)

            lipRecon = self.ReconstructLipFeature(lipFeatureVec)

            subError = []
            for sample in self.segmentationSample:
                subError.append(np.linalg.norm(lipRecon - sample))

            subError = np.array(subError, np.float64)
            # minError = np.min(subError)
            minError = np.mean(subError)
            # minError = np.var(subError)
            minError = math.log(minError)

            xSet.append(i)
            ySet.append(minError)

        return xSet, ySet

    def GetMistakeRate(self, ySet_R, ySet_G, y):
        totalPoint = len(ySet_G) + len(ySet_R)
        wrong = 0

        for point in ySet_R:
            if point < y:
                wrong += 1

        for point in ySet_G:
            if point > y:
                wrong += 1

        return float(wrong) / float(totalPoint)

    def FindBestSeparationYRG(self, ySet_R, ySet_G):
        minY = min(np.min(ySet_R), np.min(ySet_G))
        maxY = max(np.max(ySet_R), np.max(ySet_G))

        testValues = np.linspace(minY, maxY, 30)

        errorList = []

        for index, y in enumerate(testValues):
            errorList.append((y, self.GetMistakeRate(ySet_R, ySet_G, y)))

        return min(errorList, key = operator.itemgetter(1))[0]

    def FindBestSeparation(self):
        xSet_R, ySet_R, xSet_G, ySet_G, xSet, ySet = self.GetAnalytics(self.notBlankRawLip,
                                                                       self.notBlankSampleStorage.assReader)
        return self.FindBestSeparationYRG(ySet_R, ySet_G)

    def GenerateSegmentationGraph(self, xScale = 1.0):

        xSet_R, ySet_R, xSet_G, ySet_G, xSet, ySet = self.GetAnalytics(self.notBlankRawLip, self.notBlankSampleStorage.assReader)

        rawLip = self.notBlankRawLip
        assReader = self.notBlankSampleStorage.assReader

        totalLength = len(rawLip.frameList) - self.timeStep
        assert totalLength > 0
        xRange = int(math.floor(float(totalLength) * xScale))

        stop = -1
        for i in range(len(xSet_R)):
            if xSet_R[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_R = xSet_R[:stop]
            ySet_R = ySet_R[:stop]

        stop = -1
        for i in range(len(xSet_G)):
            if xSet_G[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_G = xSet_G[:stop]
            ySet_G = ySet_G[:stop]

        changingFrame = []
        lastState = self.CheckIfBlank((0, self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
        for i in range(xRange):
            currentState = self.CheckIfBlank((i, i + self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
            if currentState != lastState:
                changingFrame.append(float(i) - 0.5)
                lastState = currentState

        bestSepration = self.FindBestSeparationYRG(ySet_R, ySet_G)


        '''
        plt.axvline(x = 0.0)
        for line in changingFrame:
            plt.axvline(x = line)
        plt.axvline(x = (xRange + self.timeStep))
        '''

        '''
        plt.plot(xSet_R, ySet_R, 'r.')
        plt.plot(xSet_G, ySet_G, 'g.')
        plt.xlim((0, xRange + self.timeStep))
        plt.show()

        
        '''
        fig, ax = plt.subplots(1, 1)

        ax.axhline(bestSepration)

        ax.axvline(x=0.0)
        for line in changingFrame:
            ax.axvline(x=line)
        ax.axvline(x=(xRange + self.timeStep))

        ax.plot(xSet_R, ySet_R, 'r.')
        ax.plot(xSet_G, ySet_G, 'g.')
        ax.set_xlim((0, xRange + self.timeStep))

        return fig, ax

    def GenerateSegmentationGraphOther(self, lipTxt, lipAss, xScale = 1.0):

        rawLip = GetRawLip(lipTxt)
        assReader = AssReader()
        assReader.ReadAss(lipAss)


        xSet_R, ySet_R, xSet_G, ySet_G, xSet, ySet = self.GetAnalytics(rawLip, assReader)


        totalLength = len(rawLip.frameList) - self.timeStep
        assert totalLength > 0
        xRange = int(math.floor(float(totalLength) * xScale))

        stop = -1
        for i in range(len(xSet_R)):
            if xSet_R[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_R = xSet_R[:stop]
            ySet_R = ySet_R[:stop]

        stop = -1
        for i in range(len(xSet_G)):
            if xSet_G[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_G = xSet_G[:stop]
            ySet_G = ySet_G[:stop]

        changingFrame = []
        lastState = self.CheckIfBlank((0, self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
        for i in range(xRange):
            currentState = self.CheckIfBlank((i, i + self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
            if currentState != lastState:
                changingFrame.append(float(i) - 0.5)
                lastState = currentState

        bestSepration = self.FindBestSeparation()


        fig, ax = plt.subplots(1, 1)
        ax.axhline(bestSepration)
        ax.axvline(x = 0.0)
        for line in changingFrame:
            ax.axvline(x = line)
        ax.axvline(x = (xRange + self.timeStep))


        ax.plot(xSet_R, ySet_R, 'r.')
        ax.plot(xSet_G, ySet_G, 'g.')
        ax.set_xlim((0, xRange + self.timeStep))

        return fig, ax

    def GenerateSegmentationLineGraph(self, xScale = 1.0):
        xSet_R, ySet_R, xSet_G, ySet_G, xSet, ySet = self.GetAnalytics(self.notBlankRawLip,
                                                                       self.notBlankSampleStorage.assReader)

        rawLip = self.notBlankRawLip
        assReader = self.notBlankSampleStorage.assReader

        totalLength = len(rawLip.frameList) - self.timeStep
        assert totalLength > 0
        xRange = int(math.floor(float(totalLength) * xScale))

        stop = -1
        for i in range(len(xSet_R)):
            if xSet_R[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_R = xSet_R[:stop]
            ySet_R = ySet_R[:stop]

        stop = -1
        for i in range(len(xSet_G)):
            if xSet_G[i] > xRange:
                stop = i
                break

        if stop != -1:
            xSet_G = xSet_G[:stop]
            ySet_G = ySet_G[:stop]

        changingFrame = []
        lastState = self.CheckIfBlank((0, self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
        for i in range(xRange):
            currentState = self.CheckIfBlank((i, i + self.timeStep), rawLip.videoInfo.videoFrameTime, assReader)
            if currentState != lastState:
                changingFrame.append(float(i) - 0.5)
                lastState = currentState

        #bestSepration = self.FindBestSeparationYRG(ySet_R, ySet_G)

        '''
        plt.axvline(x = 0.0)
        for line in changingFrame:
            plt.axvline(x = line)
        plt.axvline(x = (xRange + self.timeStep))
        '''

        '''
        plt.plot(xSet_R, ySet_R, 'r.')
        plt.plot(xSet_G, ySet_G, 'g.')
        plt.xlim((0, xRange + self.timeStep))
        plt.show()


        '''
        fig, ax = plt.subplots(1, 1)

        #ax.axhline(bestSepration)

        ax.axvline(x=0.0)
        for line in changingFrame:
            ax.axvline(x=line)
        ax.axvline(x=xRange)

        ax.set_xlim((0, xRange))
        ax.set_ylim((0, xRange))

        return fig, ax

    def DeduceCluster(self, yValue, bestY):
        if yValue < bestY:
            return 0
        else:
            return 1

    def SegmentSample(self, lipTxt, smallClusterLen = 3, minSpeechDist = 4, gradientLim = 1.0 / 10.0):
        assert smallClusterLen > 1
        rawLip = GetRawLip(lipTxt)
        return self.SegmentRawSample(rawLip, smallClusterLen, minSpeechDist, gradientLim)

    def SegmentRawSample(self, rawLip, smallClusterLen = 3, minSpeechDist = 4, gradientLim = 1.0 / 10.0):
        segmentX, segmentY = self.GetSegmentIndex(rawLip)

        # cluster type
        # 0 - blank cluster
        # 1 - speech cluster

        bestY = self.FindBestSeparation()

        assert len(segmentY) > 0

        simpleCluster = []
        for i in range(len(segmentY)):
            simpleCluster.append(self.DeduceCluster(segmentY[i], bestY))

        clusters = []
        lastClusterType = simpleCluster[0]
        # begin initial clustering
        i = 0
        while i < len(segmentY):
            j = i
            while j < len(segmentY):
                if simpleCluster[j] != lastClusterType:
                    break
                j += 1
            cluster = ClusterInfo()
            cluster.left = i
            cluster.length = j - i
            cluster.type = lastClusterType
            clusters.append(cluster)

            if j >= len(segmentY): break

            lastClusterType = simpleCluster[j]
            i = j

        if len(clusters) == 1:
            return clusters

        # cancel small cluster
        i = 0

        while i < len(clusters):
            if clusters[i].length < smallClusterLen:
                hasLeft = i > 0
                hasRight = i <= len(clusters) - 1

                if hasLeft and hasRight:
                    midPoint = int(math.floor(clusters[i].length / 2))
                    rightLen = clusters[i].length - midPoint
                    clusters[i - 1].length += midPoint
                    clusters[i + 1].left -= rightLen
                    clusters[i + 1].length += rightLen
                    del clusters[i]
                    continue
                elif hasLeft and (not hasRight):
                    clusters[i - 1].length += clusters[i].length
                    del clusters[i]
                    continue
                elif hasRight and (not hasLeft):
                    clusters[i + 1].left -= clusters[i].length
                    clusters[i + 1].length += clusters[i].length
                    del clusters[i]
                    continue

            i += 1

        # refine clustering with gradient information
        for i in range(len(clusters)):
            if clusters[i].type == 1:
                assert clusters[i].length > smallClusterLen
                # find ascending positive large gradient
                GetIndex = lambda k: clusters[i].left + k
                j = 0
                while j < clusters[i].length - 1:
                    gradient = segmentY[GetIndex(j + 1)] - segmentY[GetIndex(j)]
                    if gradient < gradientLim:
                        break
                    j += 1
                lengthAfterwards = clusters[i].length - j
                if lengthAfterwards < smallClusterLen:
                    # abort
                    continue

                hasLeft = i > 0

                if hasLeft:
                    clusters[i - 1].length += j
                    clusters[i].left += j
                    clusters[i].length -= j

        # cancel small cluster
        i = 0

        while i < len(clusters):
            if clusters[i].length < smallClusterLen:
                hasLeft = i > 0
                hasRight = i <= len(clusters) - 1

                if hasLeft and hasRight:
                    midPoint = int(math.floor(clusters[i].length / 2))
                    rightLen = clusters[i].length - midPoint
                    clusters[i - 1].length += midPoint
                    clusters[i + 1].left -= rightLen
                    clusters[i + 1].length += rightLen
                    del clusters[i]
                    continue
                elif hasLeft and (not hasRight):
                    clusters[i - 1].length += clusters[i].length
                    del clusters[i]
                    continue
                elif hasRight and (not hasLeft):
                    clusters[i + 1].left -= clusters[i].length
                    clusters[i + 1].length += clusters[i].length
                    del clusters[i]
                    continue

            i += 1

        fig, ax = plt.subplots(1, 1)
        for cluster in clusters:
            ax.axvline(x=cluster.left)
        ax.axvline(x=clusters[-1].left + clusters[-1].length)

        ax.set_xlim((0, clusters[-1].left + clusters[-1].length))
        ax.set_ylim((0, clusters[-1].left + clusters[-1].length))

        '''
        # merge speech clusters
        tempClusters = []
        i = 0
        while i < len(clusters):
            if clusters[i].type == 0:
                i += 1
                continue

            j = i + 1
            found = False
            # find next speech cluster
            while j < len(clusters):
                if clusters[j].type == 1:
                    found = True
                    break
                j += 1

            cluster = clusters[i]

            if found:
                # calculate distance
                dist = clusters[j].left - clusters[i].left
                if dist < minSpeechDist:
                    # merge cluster
                    cluster = MergeClusterInfo(clusters[i], clusters[j])

            i = j + 1
            tempClusters.append(cluster)
        '''

        return clusters, fig, ax

    def CalculateStat(self):
        clusters, fig, ax = self.SegmentRawSample(self.notBlankRawLip)
        plt.close(fig)

        # extract vocal clusters
        vocalCluster = []
        for cluster in clusters:
            if cluster.type == 1:
                vocalCluster.append(cluster)

        totalNumOfVocal = len(self.notBlankSampleStorage.trainingSampleX)
        totalVocalLength = 0
        totalOverlapLength = 0
        totalNumOfCorresponding = 0

        for i in range(totalNumOfVocal):
            myFrameStart = self.notBlankSampleStorage.correspondingFrame[i][0]
            myFrameEnd = self.notBlankSampleStorage.correspondingFrame[i][1]
            totalVocalLength += myFrameEnd - myFrameStart

            overlapSample = []
            for cluster in vocalCluster:
                overlapLeft = max(myFrameStart, cluster.left)
                overlapRight = min(myFrameEnd, cluster.left + cluster.length)
                if overlapRight > overlapLeft:
                    overlapSample.append(cluster)

                if cluster.left > myFrameEnd:
                    break
                if len(overlapSample) > 1: break


            if len(overlapSample) == 1:
                totalNumOfCorresponding += 1
                overlapLeft = max(myFrameStart, overlapSample[0].left)
                overlapRight = min(myFrameEnd, overlapSample[0].left + overlapSample[0].length)
                totalOverlapLength += overlapRight - overlapLeft


        c = float(totalNumOfCorresponding) * 100.0 / float(totalNumOfVocal)
        o = float(totalOverlapLength) * 100.0 / float(totalVocalLength)

        return c, o





'''
# randomly pick some samples that are not blank
for i in range(len(notBlankStorage.trainingSampleX)):
    notBlankStorage.trainingSampleX[i] = notBlankStorage.trainingSampleX[i][:timeStep]
    notBlankStorage.trainingSampleX[i] = np.array(np.concatenate(notBlankStorage.trainingSampleX[i]).flat)

# apply pca to the dataset
# normalize dataset
mean = np.zeros(featureLength, np.float)
for i in range(len(storage.trainingSampleX)):
    mean += storage.trainingSampleX[i]
mean /= float(len(storage.trainingSampleX))

# construct sample matrix
dataMatrix = np.zeros((len(storage.trainingSampleX), featureLength), np.float)
for i in range(len(storage.trainingSampleX)):
    dataMatrix[i] = storage.trainingSampleX[i]
    dataMatrix[i] -= mean
    length = np.linalg.norm(dataMatrix[i])
    dataMatrix[i] /= length

u, d, v = np.linalg.svd(dataMatrix, False, True)

sampleSetSize = 40

def CheckIfBlank(interval, frameTime, assReader):
    leftCheck = not assReader.GetSubtitle(float(interval[0]) * frameTime)[0]
    rightCheck = not assReader.GetSubtitle(float(interval[1])* frameTime)[0]
    return leftCheck and rightCheck

def ReconstructLipSequence(lipSeq, eigenVector):
    newFeatureSize = len(eigenVector)
    ret = np.zeros(newFeatureSize, np.float)
    for i in range(newFeatureSize):
        ret[i] = np.dot(lipSeq, eigenVector[i])
    return ret

def ShowReconstruction(numOfEV):
    global v, storage
    assert numOfEV > 0
    assert numOfEV < v.shape[1]
    assert sampleSetSize <= len(storage.trainingSampleX)
    eigenVector = []

    for i in range(numOfEV):
        eigenVector.append(v[:,i])

    rawLip = GetRawLip('../Sample/sample3-15fps-lipFeature.txt')

    totalError = []
    for i in range(len(storage.trainingSampleX)):
        error = 0.0
        reconLip = ReconstructLipSequence(storage.trainingSampleX[i], eigenVector)
        for j in range(len(notBlankStorage.trainingSampleX)):
            tempReconLip = ReconstructLipSequence(notBlankStorage.trainingSampleX[j], eigenVector)
            error += np.linalg.norm(tempReconLip - reconLip)
        totalError.append((i, error))

    totalError.sort(key = operator.itemgetter(1), reverse=True)

    reconSampleSet = []
    for index, error in totalError:
        reconSampleSet.append(ReconstructLipSequence(storage.trainingSampleX[index], eigenVector))


    xSet_G = []
    ySet_G = []
    xSet_R = []
    ySet_R = []

    for i in range(storage.sampleVideoInfo.videoNumOfFrames - timeStep):
        left = i
        right = i + timeStep

        thisLipFeature = rawLip.frameList[left : right]
        lipFeatureVec = np.concatenate(thisLipFeature)
        lipFeatureVec = np.array(lipFeatureVec.flat)

        subError = []
        reconLip = ReconstructLipSequence(lipFeatureVec, eigenVector)
        for recon in reconSampleSet:
            dist = np.linalg.norm(reconLip - recon)
            subError.append(dist)

        plotError = min(subError)

        if CheckIfBlank((left, right), storage.sampleVideoInfo.videoFrameTime ,storage.assReader):
            xSet_G.append(i)
            ySet_G.append(plotError)
        else:
            xSet_R.append(i)
            ySet_R.append(plotError)

    plt.plot(xSet_G, ySet_G, 'g.')
    plt.plot(xSet_R, ySet_R, 'r.')
    plt.show()



ShowReconstruction(20)
'''

#sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt','../Sample/sample2-subtitle.ass', '../Sample/sample3-15fps-lipFeature.txt','../Sample/sample3-subtitle.ass', 4)
#sampleSegmentation.CreateSegmentationSample(30, 150, 0.2)
#sampleSegmentation.GenerateSegmentationGraph(1.0)
#sampleSegmentation.GenerateSegmentationGraphOther('../Sample/sample2-15fps-lipFeature.txt','../Sample/sample2-subtitle.ass', 0.1)
#sampleSegmentation.SegmentSample('../Sample/sample3-15fps-lipFeature.txt')

def GetSimpleSegmentationGraph():
    sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass',
                                             '../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass',
                                             4)
    sampleSegmentation.CreateSegmentationSample(35, 300)
    fig1, ax1 = sampleSegmentation.GenerateSegmentationGraphOther('../Sample/sample2-15fps-lipFeature.txt',
                                                      '../Sample/sample2-subtitle.ass', 0.08)

    fig2, ax2 = sampleSegmentation.GenerateSegmentationGraph(0.5)

    ylim1 = ax1.get_ylim()
    ylim2 = ax2.get_ylim()

    ylimMin = min(ylim1[0], ylim2[0])
    ylimMax = max(ylim1[1], ylim2[1])

    ax1.set_ylim((ylimMin, ylimMax))
    ax2.set_ylim((ylimMin, ylimMax))

    fig1.savefig('Graph/delta-sample-set.png', dpi=350, bbox_inches='tight')
    fig2.savefig('Graph/delta-test-set.png', dpi = 350, bbox_inches = 'tight')

def GetSimpleSegmentationLineGraph():
    sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass',
                                             '../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass',
                                             4)
    sampleSegmentation.CreateSegmentationSample(35, 300)
    cluster, fig, ax = sampleSegmentation.SegmentSample('../Sample/sample3-15fps-lipFeature.txt')
    ax.yaxis.set_visible(False)
    ax.set_aspect(0.25)
    print(ax.get_xlim())
    fig.savefig('Graph/segmentation-true.png', dpi = 350, bbox_inches = 'tight')

    fig, ax = sampleSegmentation.GenerateSegmentationLineGraph()
    ax.yaxis.set_visible(False)
    ax.set_aspect(0.25)
    print(ax.get_xlim())
    fig.savefig('Graph/segmentation-result.png', dpi=350, bbox_inches='tight')

def CalculateStat():
    sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass',
                                             '../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass',
                                             4)
    resultStorage = []
    for S in range(50, 450, 50):
        for J in range(5, 45, 5):
            sampleSegmentation.CreateSegmentationSample(J, S)
            c, o = sampleSegmentation.CalculateStat()
            print(repr(S) + ', ' + repr(J))
            resultStorage.append((S, J, c, o))

    with open('stat.bin', 'wb') as outfile:
        pickle.dump(resultStorage, outfile)

def StatToLaTeX():
    resultList = None

    with open('stat.bin', 'rb') as infile:
        resultList = pickle.load(infile)

    finder = dict()
    for result in resultList:
        finder[(result[0], result[1])] = (result[2], result[3])

    wholeText = ''

    for S in range(50, 450, 50):
        line = '%d & '%S
        for J in range(5, 25, 5):
            if J == 20:
                line += '%.2f\\%% & '%finder[(S,J)][0]
                line += '%.2f\\%% \\\\ \n' % finder[(S, J)][1]
            else:
                line += '%.2f\\%% & '%finder[(S,J)][0]
                line += '%.2f\\%% & '% finder[(S, J)][1]
        wholeText += line

    wholeText += '\n\n'

    for S in range(50, 450, 50):
        line = '%d & '%S
        for J in range(25, 45, 5):
            if J == 40:
                line += '%.2f\\%% & '%finder[(S,J)][0]
                line += '%.2f\\%% \\\\ \n' % finder[(S, J)][1]
            else:
                line += '%.2f\\%% & '%finder[(S,J)][0]
                line += '%.2f\\%% & '% finder[(S, J)][1]
        wholeText += line

    print(wholeText)

def ClusterToAss():
    sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass',
                                             '../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass',
                                             4)
    sampleSegmentation.CreateSegmentationSample(35, 300)
    cluster, fig, ax = sampleSegmentation.SegmentSample('../Sample/sample3-15fps-lipFeature.txt')
    ax.yaxis.set_visible(False)
    ax.set_aspect(0.25)
    print(ax.get_xlim())
    fig.savefig('Graph/segmentation-true.png', dpi=350, bbox_inches='tight')

    fig, ax = sampleSegmentation.GenerateSegmentationLineGraph()
    ax.yaxis.set_visible(False)
    ax.set_aspect(0.25)
    print(ax.get_xlim())
    fig.savefig('Graph/segmentation-result.png', dpi=350, bbox_inches='tight')


#GetSimpleSegmentationGraph()
#GetSimpleSegmentationLineGraph()
#CalculateStat()
#StatToLaTeX()