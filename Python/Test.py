import dlib
import os
import cv2
from PIL import Image
import numpy as np
from matplotlib import pyplot as plt
from LipFeatureReader import faceShapePredictorFilename
import math
from LipFeatureReader import ReadLipFeatureFromTxt
from LipFeatureProcessor import GetStandardizedVideoLipFeature
from SampleManagement import CreateSampleStorage
import keras
from keras import *
from keras.layers import *
from keras.utils import *
from keras.callbacks import *
from SampleManagement import SampleStorage, GetRawLip
import operator

from SampleSegmentation import SampleSegmentataion

def TestFaceDetector():
    detector = dlib.get_frontal_face_detector()

    faceFolder = '../Test/Face/'
    nonFaceFolder = '../Test/NonFace/'

    allFaceFile = [faceFolder + filename for filename in os.listdir(faceFolder)]
    allNonFaceFile = [nonFaceFolder + filename for filename in os.listdir(nonFaceFolder)]

    allFaceFile.sort()
    allNonFaceFile.sort()

    correctFaceCount = 0
    errorFaceList = []
    for index, faceFile in enumerate(allFaceFile):
        print('(%d) Testing %s'%(index + 1, faceFile))
        #img = cv2.imread(faceFile)
        img = None
        with open(faceFile, 'rb') as inFile:
            _img = Image.open(inFile)
            img = np.array(np.asarray(_img))
        faces = detector(img, 1)
        if len(faces) >= 1:
            correctFaceCount += 1
        else:
            errorFaceList.append(index)

    correctNonFaceCount = 0
    errorNonFaceList = []
    for index, nonFaceFile in enumerate(allNonFaceFile):
        print('(%d) Testing %s' % (index + 1, nonFaceFile))
        img = None
        with open(nonFaceFile, 'rb') as inFile:
            _img = Image.open(inFile)
            img = np.array(np.asarray(_img))
        faces = detector(img, 1)
        if len(faces) == 0:
            correctNonFaceCount += 1
        else:
            errorNonFaceList.append(index)

    pass

def ShowErrorFace():
    faceFolder = '../Test/Face/'
    allFaceFile = [faceFolder + filename for filename in os.listdir(faceFolder)]
    allFaceFile.sort()

    errorList = [4, 24, 36]

    fig, ax = plt.subplots(1, 3)

    for index, imgIndex in enumerate(errorList):
        img = None
        with open(allFaceFile[imgIndex], 'rb') as inFile:
            _img = Image.open(inFile)
            img = np.array(np.asarray(_img))
        #img = cv2.imread(allFaceFile[imgIndex])
        ax[index].imshow(img)
        ax[index].set_axis_off()

    fig.savefig('Graph/error-face-detection.png', dpi = 350, bbox_inches = 'tight')

def ShowFaceAlignment():
    faceFolder = '../Test/Face/'
    allFaceFile = [faceFolder + filename for filename in os.listdir(faceFolder)]
    allFaceFile.sort()

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(faceShapePredictorFilename)


    faceList = [0, 1, 2]
    fig, ax = plt.subplots(1, 3)

    for index, imgIndex in enumerate(faceList):
        img = None
        with open(allFaceFile[imgIndex], 'rb') as inFile:
            _img = Image.open(inFile)
            img = np.array(np.asarray(_img))
        #img = cv2.imread(allFaceFile[imgIndex])

        faces = detector(img)
        shape = predictor(img, faces[0])

        for point in shape.parts():
            cv2.circle(img, (point.x, point.y), 3, (255, 0, 0), -1)

        ax[index].imshow(img)
        ax[index].set_axis_off()


    fig.savefig('Graph/face-alignment.png', dpi = 350, bbox_inches = 'tight')

def TestFacePredictor():
    faceFolder = '../Test/Face/'
    allFaceFile = [faceFolder + filename for filename in os.listdir(faceFolder)]
    allFaceFile.sort()

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(faceShapePredictorFilename)

    deviation = []

    for index, filename in enumerate(allFaceFile):
        print('(%d) Testing %s' % (index + 1, filename))
        img = None
        with open(filename, 'rb') as inFile:
            _img = Image.open(inFile)
            img = np.array(np.asarray(_img))

        faces = detector(img)

        if len(faces) == 0: continue

        xVec = []
        yVec = []

        for i in range(50):
            shape = predictor(img, faces[0])
            shapeMat = np.zeros((68, 2), np.float64)

            for j in range(68):
                #paraX = float(shape.parts()[j].x) / float(img.shape[0])
                #paraY = float(shape.parts()[j].y) / float(img.shape[1])
                paraX = shape.parts()[j].x
                paraY = shape.parts()[j].y
                shapeMat[j][0] = paraX
                shapeMat[j][1] = paraY
            xVec.append(shapeMat[:, 0])
            yVec.append(shapeMat[:, 1])

        numOfSamples = len(xVec)
        tempVecX = np.zeros(68, np.float64)
        tempVecY = np.zeros(68, np.float64)

        for i in range(numOfSamples):
            tempVecX += xVec[i]
            tempVecY += yVec[i]

        tempVecX /= float(numOfSamples)
        tempVecY /= float(numOfSamples)

        varX = 0.0
        varY = 0.0

        for i in range(numOfSamples):
            varX += np.linalg.norm(xVec[i] - tempVecX)**2
            varY += np.linalg.norm(yVec[i] - tempVecY)**2

        varX /= float(numOfSamples)
        varY /= float(numOfSamples)

        varX = math.sqrt(varX)
        varY = math.sqrt(varY)

        deviation.append((varX, varY))

    pass

def TestVideo(txtFile):
    _lipFeature = ReadLipFeatureFromTxt(txtFile)
    lipFeature = GetStandardizedVideoLipFeature(_lipFeature)

    numOfFrames = len(lipFeature.frameList)
    featureLen = lipFeature.numOfPointPerFrame

    xVecs = []
    yVecs = []

    for index, feature in enumerate(lipFeature.frameList):
        xVec = feature[:,0]
        yVec = feature[:,1]
        xVecs.append(xVec)
        yVecs.append(yVec)

    # form matrices
    xMat = np.zeros((len(xVecs), featureLen), np.float64)
    yMat = np.zeros((len(xVecs), featureLen), np.float64)

    for i in range(len(xVecs)):
        xMat[i] = xVecs[i]
        yMat[i] = yVecs[i]

    xSD = np.zeros(featureLen, np.float64)
    ySD = np.zeros(featureLen, np.float64)

    for i in range(featureLen):
        xSD[i] = math.sqrt(np.var(xMat[:, i]))
        ySD[i] = math.sqrt(np.var(yMat[:, i]))


    def printStat(vec, name = ''):
        myMax = myMin = myMedian = myMean = 0.0
        myMax = np.max(vec)
        myMin = np.min(vec)
        myMedian = np.median(vec)
        myMean = np.mean(vec)

        print('stat for %s:\nmax: %.4E\n min: %.4E\n median: %.4E\n mean: %.4E\n'%(name, myMax, myMin, myMedian, myMean))

    printStat(xSD, 'x')
    printStat(ySD, 'y')

def GetConfusionMatrix(labelVector, trueY, predictY):
    whole = ''
    labelMap = dict()

    for label in trueY:
        if label in labelMap:
            continue
        else:
            labelMap[label] = []

    # add prediction
    for i in range(len(trueY)):
        trueLabel = trueY[i]
        predLabel = predictY[i]
        labelMap[trueLabel].append(predLabel)

    for i in range(len(labelMap)):
        if i >= len(labelMap) - 1:
            whole += r'\multicolumn{1}{c}{%s}'%labelVector[i] + ' \\\\ \n '
        else:
            whole += r'\multicolumn{1}{c}{%s}'%labelVector[i] + ' & '

    # create confusion matrix
    for i in range(len(labelMap)):
        assert i in labelMap
        line = '\\multicolumn{1}{|c}{} & ' + labelVector[i] + ' & '
        for j in range(len(labelMap)):
            if j >= len(labelMap) - 1:
                line += '%d \\\\ \\cline{3-14} \n ' % (labelMap[i].count(j))
            else:
                line += '%d & ' % (labelMap[i].count(j))
        whole += line

    print(whole)

def TestModel():
    modelFilename = '../Model/4_21_2041_model.keras'
    modelWeightFilename = '../Model/4_21_2041_weights-epoch49-acc0.9704-val_acc0.8750.hdf5'

    # load model
    model = keras.models.load_model(modelFilename)
    model.load_weights(modelWeightFilename)

    #trainSampleStorage = CreateSampleStorage('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass')
    trainSampleStorage = CreateSampleStorage('../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass')

    #kerasX, kerasY = trainSampleStorage.GetKerasAugmentedXY(30, '../Sample/sample2-15fps-lipFeature.txt')
    kerasX, kerasY = trainSampleStorage.GetKerasAugmentedXY(30, '../Sample/sample3-15fps-lipFeature.txt')

    trueY = []
    for i in range(len(kerasY)):
        thisY = kerasY[i]
        for j in range(len(thisY)):
            if abs(thisY[j] - 1.0) < 0.1 :
                trueY.append(j)
                break

    prediction = model.predict_classes(kerasX)

    GetConfusionMatrix(trainSampleStorage.labelVector, trueY, prediction)



def TestSystemOnTraining():

        trainSampleStorage = CreateSampleStorage('../Sample/sample2-15fps-lipFeature.txt',
                                                 '../Sample/sample2-subtitle.ass')

        sampleText = ''

        for i in range(len(trainSampleStorage.trainingSampleX)):
            sampleText += trainSampleStorage.labelVector[trainSampleStorage.trainingSampleY[i]] + ' \ \  '

        with open('training-text.txt', 'w') as outfile:
            outfile.write(sampleText)

        sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt',
                                                 '../Sample/sample2-subtitle.ass',
                                                 '../Sample/sample3-15fps-lipFeature.txt',
                                                 '../Sample/sample3-subtitle.ass',
                                                 4)

        sampleSegmentation.CreateSegmentationSample(35, 300)

        clusters, fig, ax = sampleSegmentation.SegmentSample('../Sample/sample2-15fps-lipFeature.txt')

        rawLip = GetRawLip('../Sample/sample2-15fps-lipFeature.txt')

        newSampleStorage = SampleStorage()
        newSampleStorage.labelVector = trainSampleStorage.labelVector
        newSampleStorage.labelMap = trainSampleStorage.labelMap
        newSampleStorage.sampleVideoInfo = trainSampleStorage.sampleVideoInfo
        newSampleStorage.subtitleAnalysisResult = trainSampleStorage.subtitleAnalysisResult
        newSampleStorage.assReader = trainSampleStorage.assReader
        newSampleStorage.neuralLip = trainSampleStorage.neuralLip
        newSampleStorage.correspondingFrame = []

        for cluster in clusters:
            if cluster.type == 1:
                clusterRange = (cluster.left + 3, cluster.left + cluster.length)
                assert clusterRange[1] > clusterRange[0]
                newLipList = rawLip.frameList[clusterRange[0]: clusterRange[1]]
                newSampleStorage.trainingSampleX.append(newLipList)
                newSampleStorage.trainingSampleY.append(0)
                newSampleStorage.correspondingFrame.append((clusterRange[0], clusterRange[1]))

        kerasX, kerasY, labels = newSampleStorage.GetKerasAugmentedXYWLabel(30, '../Sample/sample2-15fps-lipFeature.txt', 0)

        modelFilename = '../Model/4_21_2041_model.keras'
        modelWeightFilename = '../Model/4_21_2041_weights-epoch49-acc0.9704-val_acc0.8750.hdf5'

        # load model
        model = keras.models.load_model(modelFilename)
        model.load_weights(modelWeightFilename)

        predictY = [None] * len(labels)

        for i in range(len(labels)):
            labelSet = labels[i]
            myX = np.zeros((len(labelSet), kerasX.shape[1], kerasX.shape[2]), np.float64)
            for j in range(len(labelSet)):
                myX[j] = kerasX[labelSet[j]]
            prediction = model.predict_classes(myX)
            predictionList = []
            classSet = set()
            for class_num in prediction:
                classSet.add(int(class_num))
                predictionList.append(class_num)
            sortList = []
            for class_num in classSet:
                sortList.append((class_num, predictionList.count(class_num)))
            sortList.sort(key = operator.itemgetter(1), reverse=True)
            predictY[i] = sortList[0][0]

        oldWordVector = np.zeros(len(newSampleStorage.labelVector), np.float64)
        newWordVector = np.zeros(len(newSampleStorage.labelVector), np.float64)

        for i in range(len(oldWordVector)):
            oldWordVector[i] = float(trainSampleStorage.trainingSampleY.count(i))

        for i in range(len(newWordVector)):
            newWordVector[i] = float(predictY.count(i))

        sampleText = ''

        for i in range(len(predictY)):
            sampleText += trainSampleStorage.labelVector[predictY[i]] + ' \ \  '

        with open('training-text-predict.txt', 'w') as outfile:
            outfile.write(sampleText)

        print(oldWordVector)
        print(newWordVector)

        angle = math.acos(np.dot(oldWordVector, newWordVector) / (np.linalg.norm(oldWordVector) * np.linalg.norm(newWordVector)))
        angle = math.degrees(angle)
        print(angle)

        # extract vocal clusters
        vocalCluster = []
        overlapSegmentIndex = []

        for cluster in clusters:
            if cluster.type == 1:
                vocalCluster.append(cluster)

        totalNumOfVocal = len(trainSampleStorage.trainingSampleX)
        totalOverlapLength = 0
        totalNumOfCorresponding = 0

        for i in range(totalNumOfVocal):
            myFrameStart = trainSampleStorage.correspondingFrame[i][0]
            myFrameEnd = trainSampleStorage.correspondingFrame[i][1]

            overlapSample = []
            for cluster in vocalCluster:
                overlapLeft = max(myFrameStart, cluster.left)
                overlapRight = min(myFrameEnd, cluster.left + cluster.length)
                if overlapRight > overlapLeft:
                    overlapSample.append(cluster)

                if cluster.left > myFrameEnd:
                    break
                if len(overlapSample) > 1: break


            if len(overlapSample) == 1:
                overlapSegmentIndex.append(i)

        rightCount = 0
        for index in overlapSegmentIndex:
            if predictY[index] == trainSampleStorage.trainingSampleY[index]:
                rightCount+=1
        print((rightCount, totalNumOfVocal, float(rightCount)/totalNumOfVocal))

def TestSystemOnTesting():
    trainSampleStorage = CreateSampleStorage('../Sample/sample3-15fps-lipFeature.txt',
                                             '../Sample/sample3-subtitle.ass')

    sampleText = ''

    for i in range(len(trainSampleStorage.trainingSampleX)):
        sampleText += trainSampleStorage.labelVector[trainSampleStorage.trainingSampleY[i]] + ' \ \  '

    with open('training-text.txt', 'w') as outfile:
        outfile.write(sampleText)

    sampleSegmentation = SampleSegmentataion('../Sample/sample2-15fps-lipFeature.txt',
                                             '../Sample/sample2-subtitle.ass',
                                             '../Sample/sample3-15fps-lipFeature.txt',
                                             '../Sample/sample3-subtitle.ass',
                                             4)

    sampleSegmentation.CreateSegmentationSample(35, 300)

    clusters, fig, ax = sampleSegmentation.SegmentSample('../Sample/sample3-15fps-lipFeature.txt')

    rawLip = GetRawLip('../Sample/sample3-15fps-lipFeature.txt')

    newSampleStorage = SampleStorage()
    newSampleStorage.labelVector = trainSampleStorage.labelVector
    newSampleStorage.labelMap = trainSampleStorage.labelMap
    newSampleStorage.sampleVideoInfo = trainSampleStorage.sampleVideoInfo
    newSampleStorage.subtitleAnalysisResult = trainSampleStorage.subtitleAnalysisResult
    newSampleStorage.assReader = trainSampleStorage.assReader
    newSampleStorage.neuralLip = trainSampleStorage.neuralLip
    newSampleStorage.correspondingFrame = []

    for cluster in clusters:
        if cluster.type == 1:
            clusterRange = (cluster.left + 3, cluster.left + cluster.length)
            assert clusterRange[1] > clusterRange[0]
            newLipList = rawLip.frameList[clusterRange[0]: clusterRange[1]]
            newSampleStorage.trainingSampleX.append(newLipList)
            newSampleStorage.trainingSampleY.append(0)
            newSampleStorage.correspondingFrame.append((clusterRange[0], clusterRange[1]))

    kerasX, kerasY, labels = newSampleStorage.GetKerasAugmentedXYWLabel(30, '../Sample/sample3-15fps-lipFeature.txt',0)

    modelFilename = '../Model/4_21_2041_model.keras'
    modelWeightFilename = '../Model/4_21_2041_weights-epoch49-acc0.9704-val_acc0.8750.hdf5'

    # load model
    model = keras.models.load_model(modelFilename)
    model.load_weights(modelWeightFilename)

    predictY = [None] * len(labels)

    for i in range(len(labels)):
        labelSet = labels[i]
        myX = np.zeros((len(labelSet), kerasX.shape[1], kerasX.shape[2]), np.float64)
        for j in range(len(labelSet)):
            myX[j] = kerasX[labelSet[j]]
        prediction = model.predict_classes(myX)
        predictionList = []
        classSet = set()
        for class_num in prediction:
            classSet.add(int(class_num))
            predictionList.append(class_num)
        sortList = []
        for class_num in classSet:
            sortList.append((class_num, predictionList.count(class_num)))
        sortList.sort(key=operator.itemgetter(1), reverse=True)
        predictY[i] = sortList[0][0]

    oldWordVector = np.zeros(len(newSampleStorage.labelVector), np.float64)
    newWordVector = np.zeros(len(newSampleStorage.labelVector), np.float64)

    for i in range(len(oldWordVector)):
        oldWordVector[i] = float(trainSampleStorage.trainingSampleY.count(i))

    for i in range(len(newWordVector)):
        newWordVector[i] = float(predictY.count(i))

    sampleText = ''

    for i in range(len(predictY)):
        sampleText += trainSampleStorage.labelVector[predictY[i]] + ' \ \  '

    with open('training-text-predict.txt', 'w') as outfile:
        outfile.write(sampleText)

    for i in range(len(oldWordVector)):
        print(trainSampleStorage.labelVector[i])

    print(oldWordVector)
    print(newWordVector)

    angle = math.acos(
        np.dot(oldWordVector, newWordVector) / (np.linalg.norm(oldWordVector) * np.linalg.norm(newWordVector)))
    angle = math.degrees(angle)
    print(angle)

    # extract vocal clusters
    vocalCluster = []
    overlapSegmentIndex = []

    for cluster in clusters:
        if cluster.type == 1:
            vocalCluster.append(cluster)

    totalNumOfVocal = len(trainSampleStorage.trainingSampleX)
    totalOverlapLength = 0
    totalNumOfCorresponding = 0

    for i in range(totalNumOfVocal):
        myFrameStart = trainSampleStorage.correspondingFrame[i][0]
        myFrameEnd = trainSampleStorage.correspondingFrame[i][1]

        overlapSample = []
        for cluster in vocalCluster:
            overlapLeft = max(myFrameStart, cluster.left)
            overlapRight = min(myFrameEnd, cluster.left + cluster.length)
            if overlapRight > overlapLeft:
                overlapSample.append(cluster)

            if cluster.left > myFrameEnd:
                break
            if len(overlapSample) > 1: break

        if len(overlapSample) == 1:
            overlapSegmentIndex.append(i)

    rightCount = 0
    for index in overlapSegmentIndex:
        if predictY[index] == trainSampleStorage.trainingSampleY[index]:
            rightCount += 1
    print((rightCount, totalNumOfVocal, float(rightCount) / totalNumOfVocal))


#TestFaceDetector()
#ShowErrorFace()
#ShowFaceAlignment()
#TestFacePredictor()
#TestVideo('../Sample/lip-static2-lipFeature.txt')
#TestModel()
#TestSystemOnTraining()
TestSystemOnTesting()