from AssReader import AssReader
from keras import utils
from LipFeatureReader import ReadLipFeatureFromTxt
from LipFeatureProcessor import GetStandardizedVideoLipFeature, commonWaivedPoints
import math
import numpy as np


def GetFrameTime(frameCount, timePerFrame):
    return (float(frameCount) + 0.5) * timePerFrame


def GetFrameCountLowerBound(timePoint, timePerFrame):
    return math.floor(timePoint / timePerFrame)


def GetNumOfFramesOfTime(timeInterval, timePerFrame):
    return timeInterval / timePerFrame

'''
def CopyAndExtendLipSequence(target, source, timeStep):
    sourceLength = len(source)
    for i in range(sourceLength):
        target[i] = source[i].flat
    if sourceLength < timeStep:
        for i in range(sourceLength, timeStep):
            target[i] = source[sourceLength - 1].flat
'''

def CopyAndExtendLipSequence(target, source, neuralLip, timeStep):
    sourceLength = len(source)
    for i in range(sourceLength):
        target[i] = source[i].flat
    if sourceLength < timeStep:
        for i in range(sourceLength, timeStep):
            target[i] = neuralLip.flat


def GetRawLip(sampleFilename):
    _lipFeature = ReadLipFeatureFromTxt(sampleFilename)
    lipFeature = GetStandardizedVideoLipFeature(_lipFeature)
    return lipFeature

class SampleStorage:
    trainingSampleX = None
    trainingSampleY = None
    correspondingFrame = None
    labelVector = None
    labelMap = None
    sampleVideoInfo = None
    subtitleAnalysisResult = None
    assReader = None
    neuralLip = None

    def __init__(self):
        self.trainingSampleX = []
        self.trainingSampleY = []
        self.labelVector = []
        self.correspondingFrame = []
        self.labelMap = {}

    def GetKerasXY(self, timeStep):
        assert len(self.trainingSampleX) > 0
        assert len(self.trainingSampleX) == len(self.trainingSampleY)
        assert len(self.labelVector) == len(self.labelMap)

        timeStep_min = math.ceil(
            GetNumOfFramesOfTime(self.subtitleAnalysisResult['sample_length_max'], self.sampleVideoInfo.videoFrameTime))
        assert timeStep >= timeStep_min

        for sample in self.trainingSampleX:
            assert len(sample) > 0

        lipFeatureDim = len(self.trainingSampleX[0][0]) * 2

        kerasX = np.zeros((len(self.trainingSampleX), timeStep, lipFeatureDim), np.float32)
        keras_temp_Y = np.zeros((len(self.trainingSampleY)), np.float32)

        for index, sample in enumerate(self.trainingSampleX):
            CopyAndExtendLipSequence(kerasX[index], sample, self.neuralLip, timeStep)
            '''
            sampleLength = len(sample)
            for j in range(sampleLength):
                kerasX[index][j] = sample[j].flat
            if sampleLength < timeStep:
                for j in range(sampleLength, timeStep):
                    kerasX[index][j] = sample[sampleLength - 1].flat
            '''

        for index, label in enumerate(self.trainingSampleY):
            keras_temp_Y[index] = float(label)

        kerasY = utils.to_categorical(keras_temp_Y, len(self.labelVector))
        return kerasX, kerasY

    def GetKerasSegmentXY(self, timeStep):
        assert len(self.trainingSampleX) > 0
        assert len(self.trainingSampleX) == len(self.trainingSampleY)
        assert len(self.labelVector) == len(self.labelMap)

        for sample in self.trainingSampleX:
            assert len(sample) == timeStep

        lipFeatureDim = len(self.trainingSampleX[0][0]) * 2

        kerasX = np.zeros((len(self.trainingSampleX), timeStep, lipFeatureDim), np.float32)
        keras_temp_Y = np.zeros((len(self.trainingSampleY)), np.float32)

        for index, sample in enumerate(self.trainingSampleX):
            for i in range(timeStep):
                kerasX[index][i] = sample[i].flat
            '''
            sampleLength = len(sample)
            for j in range(sampleLength):
                kerasX[index][j] = sample[j].flat
            if sampleLength < timeStep:
                for j in range(sampleLength, timeStep):
                    kerasX[index][j] = sample[sampleLength - 1].flat
            '''

        for index, label in enumerate(self.trainingSampleY):
            keras_temp_Y[index] = float(label)

        kerasY = utils.to_categorical(keras_temp_Y, len(self.labelVector))
        return kerasX, kerasY

    def GetKerasAugmentedXY(self, timeStep, rawLipTxt, augment = 3):
        x, y, labels = self.GetKerasAugmentedXYWLabel(timeStep, rawLipTxt, augment)
        return x, y

    def GetKerasAugmentedXYWLabel(self, timeStep, rawLipTxt, augment = 3):
        assert len(self.trainingSampleX) > 0
        assert len(self.trainingSampleX) == len(self.trainingSampleY)
        assert len(self.labelVector) == len(self.labelMap)

        timeStep_min = math.ceil(
            GetNumOfFramesOfTime(self.subtitleAnalysisResult['sample_length_max'], self.sampleVideoInfo.videoFrameTime))
        assert timeStep >= timeStep_min

        for sample in self.trainingSampleX:
            assert len(sample) > 0

        lipFeatureDim = len(self.trainingSampleX[0][0]) * 2
        rawLip = GetRawLip(rawLipTxt)

        lipX = []
        lipY = []
        labels = []

        sampleCounter = 0
        for index, sample in enumerate(self.trainingSampleX):
            thisLipFeature = []
            labelSet = []

            sampleFrameRange = self.correspondingFrame[index]

            if augment <= 0:
                longerSample = rawLip.frameList[sampleFrameRange[0] : sampleFrameRange[1]]
                currentLip = np.zeros((timeStep, lipFeatureDim), np.float64)
                CopyAndExtendLipSequence(currentLip, longerSample, self.neuralLip, timeStep)
                thisLipFeature.append(currentLip)

            for i in range(augment):
                for j in range(augment):
                    newFrameRange = (sampleFrameRange[0] - i, sampleFrameRange[1] + j)

                    if newFrameRange[1] - newFrameRange[0] > timeStep:
                        continue
                    if newFrameRange[0] < 0:
                        continue
                    if newFrameRange[1] >= len(rawLip.frameList) - 1:
                        continue

                    longerSample = rawLip.frameList[newFrameRange[0]: newFrameRange[1]]
                    currentLip = np.zeros((timeStep, lipFeatureDim), np.float64)
                    CopyAndExtendLipSequence(currentLip, longerSample, self.neuralLip, timeStep)
                    thisLipFeature.append(currentLip)


            for lip in thisLipFeature:
                lipX.append(lip)
                lipY.append(self.trainingSampleY[index])
                labelSet.append(sampleCounter)
                sampleCounter += 1

            labels.append(labelSet)


        kerasX = np.zeros((len(lipX), timeStep, lipFeatureDim), np.float32)
        keras_temp_Y = np.zeros((len(lipY)), np.float32)

        for i in range(len(lipX)):
            kerasX[i] = lipX[i]

        for index, label in enumerate(lipY):
            keras_temp_Y[index] = float(label)

        kerasY = utils.to_categorical(keras_temp_Y, len(self.labelVector))
        return kerasX, kerasY, labels

    def GetKerasXYAndValidationXY(self, timeStep, validationFactor, forceValidation = True):
        assert len(self.trainingSampleX) > 0
        assert len(self.trainingSampleX) == len(self.trainingSampleY)
        assert len(self.labelVector) == len(self.labelMap)
        assert 0.0 <= validationFactor <= 1.0

        timeStep_min = math.ceil(
            GetNumOfFramesOfTime(self.subtitleAnalysisResult['sample_length_max'], self.sampleVideoInfo.videoFrameTime))
        assert timeStep >= timeStep_min

        for sample in self.trainingSampleX:
            assert len(sample) > 0

        lipFeatureDim = len(self.trainingSampleX[0][0]) * 2

        # for each label, pick out some frames to be validations
        sampleOfLabel = {}
        for label in range(len(self.labelVector)):
            samples = []
            for index, y in enumerate(self.trainingSampleY):
                if y == label:
                    samples.append(index)
            samples.sort()
            sampleOfLabel[label] = samples

        samplesToBeTakenOut = set()
        for label, list in sampleOfLabel.items():
            numOfValidation = round(validationFactor * float(len(list)))
            numOfValidation = min(numOfValidation, len(list) - 1)
            if forceValidation:
                numOfValidation = max(1, numOfValidation)
            for i in range(numOfValidation):
                samplesToBeTakenOut.add(list[-(i + 1)])

        keras_valX = np.zeros((len(samplesToBeTakenOut), timeStep, lipFeatureDim), np.float32)
        keras_val_temp_Y = np.zeros(len(samplesToBeTakenOut), np.float32)

        for index, sampleIndex in enumerate(samplesToBeTakenOut):
            CopyAndExtendLipSequence(keras_valX[index], self.trainingSampleX[sampleIndex], self.neuralLip, timeStep)
            keras_val_temp_Y[index] = float(self.trainingSampleY[sampleIndex])

        trainingLength = len(self.trainingSampleX) - len(samplesToBeTakenOut)

        kerasX = np.zeros((trainingLength, timeStep, lipFeatureDim), np.float32)
        keras_temp_Y = np.zeros(trainingLength, np.float32)

        currentIndex = 0
        for i in range(len(self.trainingSampleX)):
            if i in samplesToBeTakenOut:
                continue
            CopyAndExtendLipSequence(kerasX[currentIndex], self.trainingSampleX[i], self.neuralLip, timeStep)
            keras_temp_Y[currentIndex] = float(self.trainingSampleY[i])
            currentIndex += 1

        kerasY = utils.to_categorical(keras_temp_Y, len(self.labelVector))
        keras_valY = utils.to_categorical(keras_val_temp_Y, len(self.labelVector))
        return kerasX, kerasY, keras_valX, keras_valY


def AnalyzeSubtitle(sampleSubtitle):
    assReader = AssReader()
    assReader.ReadAss(sampleSubtitle)

    sampleLengthList = []
    sampleIntervalList = []

    labelSet = set()
    labelVector = []
    labelMap = {}

    for subtitle in assReader.mySubtitle:
        sampleLengthList.append(subtitle.endTimePoint - subtitle.startTimePoint)
        labelSet.add(subtitle.content)

    for i in range(len(assReader.mySubtitle) - 1):
        sampleIntervalList.append(assReader.mySubtitle[i + 1].startTimePoint - assReader.mySubtitle[i].endTimePoint)

    for item in labelSet:
        labelVector.append(item)

    # sort to ensure consistency
    labelVector.sort()

    for i in range(len(labelVector)):
        labelMap[labelVector[i]] = i

    print('subtitle analysis result for %s' % sampleSubtitle)
    print('sample length:\nmax:%f\nmin:%f' % (max(sampleLengthList), min(sampleLengthList)))
    print('samle interval:\nmax:%f\nmin:%f' % (max(sampleIntervalList), min(sampleIntervalList)))
    print('number of labels: %d' % len(labelVector))

    result = {}
    result['sample_length_max'] = max(sampleLengthList)
    result['sample_length_min'] = min(sampleLengthList)
    result['sample_interval_max'] = max(sampleIntervalList)
    result['sample_interval_min'] = min(sampleIntervalList)

    return assReader, result, labelVector, labelMap


def CreateSampleStorage(sampleFilename, sampleSubtitleFilename):
    assReader, subtitleAnalysisResult, labelVector, labelMap = AnalyzeSubtitle(sampleSubtitleFilename)

    _lipFeature = ReadLipFeatureFromTxt(sampleFilename)
    lipFeature = GetStandardizedVideoLipFeature(_lipFeature)

    sampleStorage = SampleStorage()
    sampleStorage.sampleVideoInfo = lipFeature.videoInfo
    sampleStorage.subtitleAnalysisResult = subtitleAnalysisResult
    sampleStorage.labelVector = labelVector
    sampleStorage.labelMap = labelMap
    sampleStorage.assReader = assReader

    foundNeural = False
    neuralLip = None

    for i in range(len(lipFeature.frameList)):
        time = GetFrameTime(i, lipFeature.videoInfo.videoFrameTime)
        if assReader.GetSubtitle(time)[0]:
            continue
        else:
            foundNeural = True
            neuralLip = lipFeature.frameList[i]
            break

    sampleStorage.neuralLip = neuralLip

    for subtitle in assReader.mySubtitle:
        currentSample = []
        nowFrame = GetFrameCountLowerBound(subtitle.startTimePoint, lipFeature.videoInfo.videoFrameTime)
        startFrame = None
        endFrame = None
        started = False
        while True:
            nowTime = GetFrameTime(nowFrame, lipFeature.videoInfo.videoFrameTime)
            if nowTime < subtitle.startTimePoint:
                pass
            elif nowTime > subtitle.endTimePoint:
                endFrame = nowFrame
                break
            else:
                if not started:
                    startFrame = nowFrame
                    started = True
                currentSample.append(lipFeature.frameList[nowFrame])

            nowFrame += 1

        sampleStorage.trainingSampleX.append(currentSample)
        sampleStorage.trainingSampleY.append(labelMap[subtitle.content])
        sampleStorage.correspondingFrame.append((startFrame, endFrame))

    return sampleStorage

# label1 = black segment
def CreateSegmentationSampleStorage(sampleFilename, sampleSubtitleFilename, timeStep):
    assReader, subtitleAnalysisResult, labelVector, labelMap = AnalyzeSubtitle(sampleSubtitleFilename)

    _lipFeature = ReadLipFeatureFromTxt(sampleFilename)
    lipFeature = GetStandardizedVideoLipFeature(_lipFeature)

    assert len(lipFeature.frameList) >= timeStep

    sampleStorage = SampleStorage()
    sampleStorage.sampleVideoInfo = lipFeature.videoInfo
    sampleStorage.subtitleAnalysisResult = subtitleAnalysisResult
    sampleStorage.assReader = assReader

    assert timeStep * lipFeature.videoInfo.videoFrameTime <= subtitleAnalysisResult['sample_interval_min']

    sampleStorage.neuralLip = None

    numOfSamples = len(lipFeature.frameList) - timeStep

    for i in range(numOfSamples):
        left = i
        right = i + timeStep

        leftTime = GetFrameTime(left, lipFeature.videoInfo.videoFrameTime)
        rightTime = GetFrameTime(right, lipFeature.videoInfo.videoFrameTime)

        leftCheck = not assReader.GetSubtitle(leftTime)[0]
        rightCheck = not assReader.GetSubtitle(rightTime)[0]

        label = None
        if leftCheck and rightCheck:
            label = 1
        else:
            label = 0

        currentSample = lipFeature.frameList[left : right]
        sampleStorage.trainingSampleX.append(currentSample)
        sampleStorage.trainingSampleY.append(label)
        sampleStorage.correspondingFrame.append((left, right))


    sampleStorage.labelVector = [0,1]
    sampleStorage.labelMap = {0:0, 1:1}

    return sampleStorage

# label1 = black segment
def CreateBlankSampleStorage(sampleFilename, sampleSubtitleFilename, timeStep):
    assReader, subtitleAnalysisResult, labelVector, labelMap = AnalyzeSubtitle(sampleSubtitleFilename)

    _lipFeature = ReadLipFeatureFromTxt(sampleFilename)
    lipFeature = GetStandardizedVideoLipFeature(_lipFeature)

    assert len(lipFeature.frameList) >= timeStep

    sampleStorage = SampleStorage()
    sampleStorage.sampleVideoInfo = lipFeature.videoInfo
    sampleStorage.subtitleAnalysisResult = subtitleAnalysisResult
    sampleStorage.assReader = assReader

    assert timeStep * lipFeature.videoInfo.videoFrameTime <= subtitleAnalysisResult['sample_interval_min']

    sampleStorage.neuralLip = None

    numOfSamples = len(lipFeature.frameList) - timeStep


    for i in range(numOfSamples):
        left = i
        right = i + timeStep

        leftTime = GetFrameTime(left, lipFeature.videoInfo.videoFrameTime)
        rightTime = GetFrameTime(right, lipFeature.videoInfo.videoFrameTime)

        leftCheck = not assReader.GetSubtitle(leftTime)[0]
        rightCheck = not assReader.GetSubtitle(rightTime)[0]

        label = None
        if leftCheck and rightCheck:
            label = 0
        else:
            continue

        currentSample = lipFeature.frameList[left : right]
        # flatten the dataset

        totalSample = np.concatenate(currentSample)
        sampleStorage.trainingSampleX.append(np.array(totalSample.flat))
        sampleStorage.trainingSampleY.append(label)
        sampleStorage.correspondingFrame.append((left, right))



    sampleStorage.labelVector = [0]
    sampleStorage.labelMap = {0: 0, 1: 1}

    return sampleStorage
