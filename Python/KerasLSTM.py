from SampleManagement import CreateSampleStorage
import keras
from keras import *
from keras.layers import *
from keras.utils import *
from keras.callbacks import *
import numpy as np
import codecs
import time

timeStep = 30
storage = CreateSampleStorage('../Sample/sample2-15fps-lipFeature.txt', '../Sample/sample2-subtitle.ass')
valStorage = CreateSampleStorage('../Sample/sample3-15fps-lipFeature.txt', '../Sample/sample3-subtitle.ass')
# equalize the neural lip
valStorage.neuralLip = storage.neuralLip

lipFeatureDim = len(storage.trainingSampleX[0][0]) * 2

#X, Y = storage.GetKerasXY(timeStep)
X, Y = storage.GetKerasAugmentedXY(timeStep, '../Sample/sample2-15fps-lipFeature.txt')
valX, valY = valStorage.GetKerasAugmentedXY(timeStep, '../Sample/sample3-15fps-lipFeature.txt')

model = Sequential()
model.add(LSTM(64, return_sequences=True, input_shape=(timeStep, lipFeatureDim)))
model.add(GaussianDropout(0.1))
model.add(LSTM(64, return_sequences=True))
model.add(GaussianDropout(0.1))
#model.add(LSTM(32, return_sequences=True))
model.add(LSTM(64, return_sequences=True))
model.add(GaussianDropout(0.1))
model.add(LSTM(64))
model.add(Dense(len(storage.labelVector), activation='softmax'))

nowTime = time.localtime()
dateString = '%d_%d_%02d%02d_'%(nowTime[1], nowTime[2], nowTime[3], nowTime[4])
weightsFilepath = '../Model/%sweights-epoch{epoch:d}-acc{acc:.4f}-val_acc{val_acc:.4f}.hdf5'%dateString
modelFilepath = '../Model/%smodel.keras'%dateString

model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
'''
modelYAML = model.to_yaml()
with open(modelFilepath, 'w') as outFile:
    outFile.write(modelYAML)
'''

model.save(modelFilepath)

valcheckPoint = ModelCheckpoint(weightsFilepath, monitor='val_acc', verbose=0, mode='max', save_best_only = True)
checkPoint = ModelCheckpoint(weightsFilepath, monitor='acc', verbose=0, mode='max', save_best_only = True)
callbackList = [checkPoint, valcheckPoint]

model.fit(X, Y, epochs=1000, validation_data=(valX, valY), verbose=2, callbacks=callbackList)

