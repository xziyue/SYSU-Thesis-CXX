import cv2
import dlib
import numpy as np
import pickle
import time
import math
import threading
import multiprocessing
import os

faceShapePredictorFilename = 'shape_predictor_68_face_landmarks.dat'
resampleTimes = 10

assert resampleTimes > 0

def DlibPointToNumpyPoint(dlibPoint, dtype=np.int):
    pointTup = (dlibPoint.x, dlibPoint.y)
    return np.array((pointTup), dtype)

def GetFaceLandmark(_points):
    points = np.zeros((len(_points), 2), np.float64)

    # convert into numpy array
    for i in range(len(_points)):
        points[i] = DlibPointToNumpyPoint(_points[i], np.float64)

    return {
        "chin": points[0:17],
        "left_eyebrow": points[17:22],
        "right_eyebrow": points[22:27],
        "nose_bridge": points[27:31],
        "nose_tip": points[31:36],
        "left_eye": points[36:42],
        "right_eye": points[42:48],
        "lip": points[48:68]
    }

lipFeatureLength = 68 - 48

def AssignTask(numOfTask, numOfPart):
    assert numOfTask > 0
    assert numOfPart > 0
    ret = [None] * numOfPart

    if numOfPart >= numOfTask:
        raise RuntimeError('not supported assignment scenario')
    else:
        stepSize = math.floor(numOfTask / numOfPart)
        for i in range(numOfPart - 1):
            ret[i] = (i * stepSize, (i + 1) * stepSize)
        ret[numOfPart - 1] = ((numOfPart - 1) * stepSize, numOfTask)

    return ret

class VideoInfo:
    videoFilename = ''
    videoFPS = 0.0
    videoFrameTime = 0.0
    videoNumOfFrames = 0

class VideoLipFeature:
    videoInfo = None
    frameList = None
    numOfPointPerFrame = 0

    def __init__(self):
        self.videoInfo = VideoInfo()
        self.frameList = []

def GetVideoInfo(filename):
    cap = cv2.VideoCapture(filename)
    if not cap.isOpened():
        raise RuntimeError('unable to open video file %s' % filename)
    videoInfo = VideoInfo()
    videoInfo.videoFilename = filename
    videoInfo.videoFPS = math.floor(cap.get(cv2.CAP_PROP_FPS))
    assert abs(float(videoInfo.videoFPS) - cap.get(cv2.CAP_PROP_FPS)) <= 1.0e-3
    videoInfo.videoFrameTime = 1.0 / videoInfo.videoFPS
    videoInfo.videoNumOfFrames = math.floor(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    assert abs(float(videoInfo.videoNumOfFrames) - cap.get(cv2.CAP_PROP_FRAME_COUNT)) <= 1.0e-3
    cap.release()
    return videoInfo


def ExtractTask(myThreadID, myTask, videoInfo, progressList, resultList, resultListLock):
    print('thread %i has started'%myThreadID)
    cap = cv2.VideoCapture(videoInfo.videoFilename)
    cap.set(cv2.CAP_PROP_POS_FRAMES, myTask[0])
    taskCount = myTask[1] - myTask[0]

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(faceShapePredictorFilename)

    result = []

    for i in range(taskCount):
        progressList[myThreadID] = i
        assert cap.isOpened()
        hasFrame, frame = cap.read()
        assert hasFrame

        faces = detector(frame, 1)

        if len(faces) == 0:
            print('can not find face in frame %d, inserting blank lip feature' % (myTask[0] + i))
            result.append(None)
        else:
            currentFeature = np.zeros((lipFeatureLength, 2), np.float64)

            for i in range(resampleTimes):
                shape = predictor(frame, faces[0])
                landmark = GetFaceLandmark(shape.parts())
                currentFeature += landmark['lip']

            currentFeature /= float(resampleTimes)

            result.append(currentFeature)

    resultListLock.acquire()
    resultList[myThreadID] = result
    resultListLock.release()

def ExtractLipFeatureFromVideo(filename, outputFilename, outputTimeInterval=10.0):
    videoInfo = GetVideoInfo(filename)

    taskCount = videoInfo.videoNumOfFrames
    taskParts = multiprocessing.cpu_count()
    assert taskParts > 0

    taskAssignment = AssignTask(taskCount, taskParts)

    # no lock on progress list
    progressList = [0] * taskParts
    resultList = [None] * taskParts
    resultListLock = threading.Lock()

    startTime = time.time()

    for i in range(taskParts):
        t = threading.Thread(target=ExtractTask,
                             args=(i, taskAssignment[i], videoInfo, progressList, resultList, resultListLock))
        t.start()

    lastOutputTime = startTime - (0.8 * outputTimeInterval)
    while True:
        nowTime = time.time()
        timeDifference = nowTime - startTime

        # scan the result list to see whether the task has finished
        resultListLock.acquire()
        allFinished = True
        for i in range(taskParts):
            if resultList[i] is None:
                allFinished = False
                break
        resultListLock.release()

        if allFinished:
            print('lip feature extraction finished in %.2f seconds' % timeDifference)
            lipFeature = VideoLipFeature
            lipFeature.videoInfo = videoInfo
            for i in range(taskParts):
                lipFeature.frameList += resultList[i]
            with open(outputFilename, 'wb') as outfile:
                pickle.dump(lipFeature, outfile)
            print('lip feature saved to %s' % os.path.abspath(outputFilename))
            break
        else:
            outputTimeDifference = nowTime - lastOutputTime
            if outputTimeDifference > outputTimeInterval:
                frameCount = 0
                # might be inaccurate due to resource collision
                for i in range(taskParts):
                    frameCount += progressList[i]
                progress = float(frameCount) * 100.0 / float(videoInfo.videoNumOfFrames)
                speed = float(frameCount) / timeDifference
                estimation = float(videoInfo.videoNumOfFrames - frameCount) / speed

                print('%d/%d (%.2f%%) | time used: %.2fs | estimated time left: %.2fs' % (
                frameCount, videoInfo.videoNumOfFrames, progress, timeDifference, estimation))
                lastOutputTime = nowTime

            else:
                time.sleep(25)

def GetNumpyPointFromText(txt):
    comma = txt.find(',')
    assert comma != -1
    x = float(txt[:comma])
    y = float(txt[comma+1:])
    return np.array((x,y), np.float64)

def ReadLipFeatureFromTxt(filename):
    lines = None
    with open(filename, 'r') as infile:
        lines = infile.readlines()

    for i in range(len(lines)):
        lines[i] = lines[i].strip('\n').strip('\r')

    propertyLine = lines.index('[Property]')
    frameLine = lines.index('[Frame]')

    if propertyLine == -1 or frameLine == -1:
        raise RuntimeError('invalid lip feature file')

    # build property map
    propertyMap = {}
    for i in range(propertyLine + 1, frameLine):
        line = lines[i]
        if len(line) == 0: continue
        equalsSign = line.find('=')
        assert equalsSign != -1
        propertyName = line[:equalsSign]
        propertyVal = line[equalsSign+1:]
        propertyMap[propertyName] = propertyVal

    lipFeature = VideoLipFeature()
    lipFeature.videoInfo.videoFilename = propertyMap['videoFilename']
    lipFeature.videoInfo.videoFPS = int(propertyMap['videoFPS'])
    lipFeature.videoInfo.videoFrameTime = float(propertyMap['videoFrameTime'])
    lipFeature.videoInfo.videoNumOfFrames = int(propertyMap['videoNumOfFrame'])
    lipFeature.numOfPointPerFrame = int(propertyMap['numOfPointPerFrame'])

    thisLipFeatureLength = int(propertyMap['numOfPointPerFrame'])
    assert thisLipFeatureLength == lipFeatureLength

    frameCounter = 0

    # read frames
    for i in range(frameLine + 1, len(lines)):
        line = lines[i]
        if len(line) == 0 : continue
        spacePos = line.find(' ')
        assert spacePos != -1
        equalsSign = line[:spacePos].find('=')
        assert equalsSign != -1
        frameID = int(line[:spacePos][equalsSign + 1:])
        if frameID != frameCounter:
            print(frameID)
            print(frameCounter)
        assert frameID == frameCounter

        pointsTxt = line[spacePos + 1:]
        if len(pointsTxt) == 0:
            lipFeature.frameList.append(None)
        else:
            points = pointsTxt.split(';')[:-1]
            assert len(points) == thisLipFeatureLength
            thisLip = np.zeros((lipFeatureLength, 2), np.float64)
            for index, point in enumerate(points):
                thisLip[index] = GetNumpyPointFromText(point).astype(np.float64)
            lipFeature.frameList.append(thisLip)
        frameCounter += 1


    return lipFeature

def ReadNeutralLipFeature():
    path = '../Sample/sample2-neutral.jpg'
    img = cv2.imread(path)

    detector = dlib.get_frontal_face_detector()
    predictor = dlib.shape_predictor(faceShapePredictorFilename)

    faces = detector(img, 1)
    assert len(faces) == 1

    shape = predictor(img, faces[0])

    landmark = GetFaceLandmark(shape.parts())
    currentFeature = landmark['lip']

    feature = VideoLipFeature()
    feature.videoInfo = VideoInfo()
    feature.frameList.append(currentFeature)

    return feature


