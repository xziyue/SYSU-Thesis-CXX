import math

txtFilename = r'C:\Users\xziyu\OneDrive\Thesis\Sample\sample2-subtitle-auto.txt'
outputFileName = r'C:\Users\xziyu\OneDrive\Thesis\Sample\sample2-subtitle-auto.ass'

lines = []

with open(txtFilename, 'r') as infile:
    lines = infile.readlines()

data = []

for i in range(len(lines)):
    lines[i] = lines[i].rstrip('\n').rstrip('\r')
    splited = lines[i].split(' ')
    start = float(splited[0])
    end = float(splited[1])
    data.append((start, end))


frontText = '[Events]\nFormat: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text'

def GetNewLine(startTimeString, endTimeString):
   return 'Dialogue: 0,%s,%s,Default,,0,0,0,,\n'%(startTimeString, endTimeString)

def ConvertTimeToString(timePoint):
    hour = math.floor(timePoint / 3600.0)
    timePoint -= float(hour * 3600.0)
    minute = math.floor(timePoint / 60.0)
    timePoint -= float(minute * 60.0)
    return '%d:%d:%.2f'%(hour, minute, timePoint)

finalString = ''
finalString += frontText
for dataLine in data:
    startStr = ConvertTimeToString(dataLine[0])
    endStr = ConvertTimeToString(dataLine[1])
    finalString += GetNewLine(startStr, endStr)

with open(outputFileName, 'w') as outfile:
    outfile.write(finalString)
