import codecs

class AssTime:
    hour = 0
    minute = 0
    second = 0.0

    def GetTimePoint(self):
        return float(self.hour * 3600 + self.minute * 60) + self.second

    def GetAssTime(timePoint):
        timePoint = float(timePoint)
        assTime = AssTime()
        assTime.hour = int(timePoint / 3600.0)
        timePoint -= assTime.hour * 3600.0
        assTime.minute = int(timePoint / 60.0)
        timePoint -= assTime.minute * 60.0
        assTime.second = timePoint
        return assTime

def TimeStrToAssTime(timeStr):
    content = timeStr.split(':')
    assTime = AssTime()
    assTime.hour = int(content[0])
    assTime.minute = int(content[1])
    assTime.second = float(content[2])
    return assTime.GetTimePoint()

class AssSubtitle:
    startTimePoint = 0.0
    endTimePoint = 0.0
    content = ''

class AssReader:

    mySubtitle = None

    def __init__(self):
        self.mySubtitle = []

    def CheckIntegrity(self):
        if len(self.mySubtitle) == 0: return

        # the time of subtitles must be valid
        for subtitle in self.mySubtitle:
            assert subtitle.endTimePoint >= subtitle.startTimePoint

        # do not allow overlapping subtitles
        for index in range(len(self.mySubtitle) - 1):
            assert self.mySubtitle[index].endTimePoint <= self.mySubtitle[index + 1].startTimePoint

    def ReadAss(self, filename):
        self.mySubtitle.clear()

        with codecs.open(filename, 'r', 'utf8') as infile:
            lines = infile.readlines()

            # format the lines
            for index in range(len(lines)):
                lines[index] = lines[index].strip('\n').strip('\r')

            dialogueIndex = -1
            # find the index of the first dialog line
            for index, line in enumerate(lines):
                if line.startswith('Dialogue:'):
                    dialogueIndex = index
                    break
            assert dialogueIndex > -1

            # extract time and subtitles
            for line in lines[dialogueIndex:]:
                if len(line) == 0:continue
                content = line.split(',')
                startTimeStr = content[1]
                endTimeStr = content[2]
                subtitle = ''.join(content[9:])
                startTimePoint = TimeStrToAssTime(startTimeStr)
                endTimePoint = TimeStrToAssTime(endTimeStr)

                assSubtitle = AssSubtitle()
                assSubtitle.startTimePoint = startTimePoint
                assSubtitle.endTimePoint = endTimePoint
                assSubtitle.content = subtitle
                self.mySubtitle.append(assSubtitle)

            GetStart = lambda subtitle : subtitle.startTimePoint
            self.mySubtitle.sort(key = GetStart)
            self.CheckIntegrity()

    # returns the subtitle given time point
    # returns a 2-tuple, the first value indicates
    # whether there is subtitle in the given time point;
    # the second value is the subtitle (None if there is no subtitle)
    def GetSubtitle(self, timePoint):
        if len(self.mySubtitle) == 0:
            return False, None

        # binary search the subtitle
        start = 0
        end = len(self.mySubtitle) - 1

        while end > start:
            mid = int((start + end) / 2)
            if timePoint < self.mySubtitle[mid].startTimePoint:
                end = mid - 1
            elif timePoint > self.mySubtitle[mid].endTimePoint:
                start = mid + 1
            else:
                return True, self.mySubtitle[mid].content

        if end == start:
            if self.mySubtitle[start].startTimePoint <= timePoint <= self.mySubtitle[start].endTimePoint:
                return True, self.mySubtitle[start].content

        return False, None


    def GetAssSubtitle(self, timePoint):
        if len(self.mySubtitle) == 0:
            return False, None

        # binary search the subtitle
        start = 0
        end = len(self.mySubtitle) - 1

        while end > start:
            mid = int((start + end) / 2)
            if timePoint < self.mySubtitle[mid].startTimePoint:
                end = mid - 1
            elif timePoint > self.mySubtitle[mid].endTimePoint:
                start = mid + 1
            else:
                return True, self.mySubtitle[mid]

        if end == start:
            if self.mySubtitle[start].startTimePoint <= timePoint <= self.mySubtitle[start].endTimePoint:
                return True, self.mySubtitle[start]

        return False, None
