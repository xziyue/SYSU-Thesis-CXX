from LipFeatureReader import ReadLipFeatureFromTxt
from LipFeatureProcessor import GetStandardizedVideoLipFeature, GetTruncatedLipFeature
import cv2
import numpy as np
from matplotlib import pyplot as plt

def ShowLipFeatureAnimation(lipFeature, windowSize):
    for frameID, frame in enumerate(lipFeature.frameList):
        newImg = np.empty(windowSize, np.uint8)
        newImg.fill(255)

        newLip = frame[:] * np.array(windowSize, np.float64)
        newLip = np.round(newLip).astype(np.int)

        for i in range(lipFeature.numOfPointPerFrame):
            cv2.circle(newImg, (newLip[i][0], newLip[i][1]), 4, (0,0,255), -1)

        cv2.putText(newImg, repr(frameID + 1), (20,50), cv2.FONT_HERSHEY_PLAIN, 1, (0,0,0), 2)

        cv2.imshow('lip animation', newImg)

        if cv2.waitKey(int(lipFeature.videoInfo.videoFrameTime * 1000.0)) == ord('q'):
            break
    cv2.destroyAllWindows()

def GetLipFeatureAnimation(lipFeatureList, windowSize):
    imgSeq = []
    assert len(lipFeatureList) > 0
    for frameID, frame in enumerate(lipFeatureList):
        newImg = np.empty(windowSize, np.uint8)
        newImg.fill(255)

        newLip = frame[:] * np.array(windowSize, np.float64)
        newLip = np.round(newLip).astype(np.int)

        for i in range(len(newLip)):
            cv2.circle(newImg, (newLip[i][0], newLip[i][1]), 4, (0,0,255), -1)

        cv2.putText(newImg, repr(frameID + 1), (20, 50), cv2.FONT_HERSHEY_PLAIN, 1, (0, 0, 0), 2)

        imgSeq.append(newImg)
    return imgSeq


def ShowLipFeatureOverlay(videoFilename, lipFeature):
    cap = cv2.VideoCapture(videoFilename)

    frameCounter = 0
    while cap.isOpened():
        hasFrame, frame = cap.read()

        currentLip = lipFeature.frameList[frameCounter].astype(np.int)

        for i in range(lipFeature.numOfPointPerFrame):
            cv2.circle(frame, (currentLip[i][0], currentLip[i][1]), 3, (0,0,255), -1)

        cv2.imshow('lip overlay', frame)

        if cv2.waitKey(int(lipFeature.videoInfo.videoFrameTime * 1000.0)) == ord('q'):
            break

        frameCounter += 1

    cv2.destroyAllWindows()


def SaveLipFeatureOverlay(videoFilename, lipFeature):
    cap = cv2.VideoCapture(videoFilename)

    frameCounter = 0
    while cap.isOpened():
        if frameCounter >= 30:
            break

        hasFrame, frame = cap.read()

        frameSize = np.array(frame.shape, np.float)
        frameSize = np.round(frameSize * 0.6).astype(np.int)
        tFrameSize = ((frameSize[1], frameSize[0]))

        currentLip = lipFeature.frameList[frameCounter].astype(np.int)

        for i in range(lipFeature.numOfPointPerFrame):
            cv2.circle(frame, (currentLip[i][0], currentLip[i][1]), 3, (0,0,255), -1)

        frame = cv2.resize(frame, tFrameSize)
        filename = '../Slide/2018-4-1/lfoverlay_%d.jpg'%frameCounter

        cv2.imwrite(filename, frame)

        frameCounter += 1

    cv2.destroyAllWindows()

def SaveTargetFrameOverlay(videoFilename, lipFeature, unLip, frameNumber):
    cap = cv2.VideoCapture(videoFilename)

    assert cap.isOpened()

    cap.set(cv2.CAP_PROP_POS_FRAMES, frameNumber)

    hasFrame, frame = cap.read()

    assert hasFrame

    currentLip = unLip.frameList[frameNumber].astype(np.int)

    for i in range(unLip.numOfPointPerFrame):
        cv2.circle(frame, (currentLip[i][0], currentLip[i][1]), 3, (0, 0, 255), -1)

    #cv2.imwrite('../fa-overlay.png', frame)

    xSet = []
    ySet = []

    for point in lipFeature.frameList[frameNumber]:
        xSet.append(point[0])
        ySet.append(point[1])

    plt.plot(xSet, ySet, 'r.')
    plt.xlim((0.0, 1.0))
    plt.ylim((1.0, 0.0))
    plt.savefig('../fa-points.png', dpi = 350, bbox_inches = 'tight')
    #plt.show()


lipTxtFilename = '../Sample/lip-static1-lipFeature.txt'
#videoSampleFilename = '../Sample/sample3-15fps.mp4'

unprocessedLipFeature = ReadLipFeatureFromTxt(lipTxtFilename)
lipFeature = GetStandardizedVideoLipFeature(unprocessedLipFeature)
#unprocessedLipFeature = GetTruncatedLipFeature(unprocessedLipFeature)

ShowLipFeatureAnimation(lipFeature, (500, 500))
#ShowLipFeatureOverlay(videoSampleFilename, unprocessedLipFeature)
#SaveLipFeatureOverlay(videoSampleFilename, unprocessedLipFeature)
#SaveTargetFrameOverlay(videoSampleFilename, lipFeature, unprocessedLipFeature, 28)