#pragma once

#include <algorithm>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <memory>
#include <limits>
#include <set>

using namespace std;

#include "FLAC++/decoder.h"

template<typename Error = runtime_error>
inline void Assert(bool expr, const string &info = string{}) {
	if (!expr) {
		throw Error{ info };
	}
}

#pragma comment(lib, "libogg_static.lib")
#pragma comment(lib, "libFLAC++_static.lib")
#pragma comment(lib, "libFLAC_static.lib")