#pragma once

#include "include.h"

using AudioDataElementType = float;
using AudioDataType = vector<AudioDataElementType>;


struct AudioData {
	int sampleRate{ 0 };
	AudioDataType audioData;
};

class FLACDecoder : public FLAC::Decoder::File {
public:
	using BaseClass = FLAC::Decoder::File;
	FLACDecoder(){}

	FLACDecoder(const FLACDecoder&) = delete;
	FLACDecoder &operator = (const FLACDecoder&) = delete;

	int totalSamples{ 0 };
	int sampleRate{ 0 };
	int bitsPerSample{ 0 };

	AudioDataType audioData;

protected:
	virtual ::FLAC__StreamDecoderWriteStatus write_callback(const ::FLAC__Frame *frame, const FLAC__int32 * const buffer[]);
	virtual void metadata_callback(const ::FLAC__StreamMetadata *metadata);
	virtual void error_callback(::FLAC__StreamDecoderErrorStatus status);
};

AudioData ReadFLACFile(const string &filename);