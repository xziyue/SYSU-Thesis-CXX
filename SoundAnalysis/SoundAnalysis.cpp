#include "helper.h"
#include <cstdlib>

const string soundPath{ R"(C:\Users\xziyu\OneDrive\Thesis\Sample\sample2-30fps_audio.flac)" };

struct TimeInterval {
	double start;
	double end;
};

int main(int argc, const char **argv)
{

	auto audioData = move(ReadFLACFile(soundPath));

	Assert(audioData.sampleRate % 100 == 0, "sample rate not supported");

	auto integratePerSecond = audioData.sampleRate / 100;

	double timePerBucket = 0.1;

	auto totalIntegrationBuckets = (int)audioData.audioData.size() / integratePerSecond;

	cout << totalIntegrationBuckets << endl;

	vector<float> integrationResult;
	integrationResult.reserve(totalIntegrationBuckets);

	for (auto i = 0; i < totalIntegrationBuckets; ++i) {
		float result = 0.0f;
		for (auto j = i * integratePerSecond; j < (i + 1) * integratePerSecond; ++j) {
			result += abs(audioData.audioData[j]);
		}
		integrationResult.push_back(result);
	}

	auto durationLength = 5;
	Assert((int)integrationResult.size() > durationLength, "audio sequence too short");

	vector<float> rangeIntegration;
	rangeIntegration.resize(integrationResult.size() - durationLength, 0.0f);

	for (auto i = 0; i < (int)rangeIntegration.size(); ++i) {
		for (auto j = 0; j < durationLength; ++j) {
			rangeIntegration[i] += integrationResult[i + j];
		}
	}

	auto minInt = *min_element(rangeIntegration.begin(), rangeIntegration.end());
	auto maxInt = *max_element(rangeIntegration.begin(), rangeIntegration.end());

	cout << minInt << endl;
	cout << maxInt << endl;

	vector<double> histogram;
	histogram.resize(30);

	for (auto i = 0; i < (int)histogram.size(); ++i) {
		float valve = maxInt / (float)histogram.size() * (float)i;
		int count = 0;
		for (auto j = 0; j < (int)rangeIntegration.size(); ++j) {
			if (rangeIntegration[j] > valve) {
				count++;
			}
		}
		histogram[i] = (double)count / (double)rangeIntegration.size() * 100.0;
	}

	for (auto i = 0; i < (int)histogram.size(); ++i) {
		cout << "bin " << i << " " << histogram[i] << endl;
	}

	float rejectInterval = 10.0f / 300.0f;

	vector<int> goodRange;
	goodRange.reserve(rangeIntegration.size());

	// screen those blank ranges
	for (auto i = 0; i < rangeIntegration.size(); ++i) {
		if (rangeIntegration[i] >= rejectInterval) {
			goodRange.push_back(i);
		}
	}

	vector<TimeInterval> speechInterval;

	// concatenate consecutive ranges
	for (auto i = 0; i < goodRange.size(); ++i) {
		auto lastNum = goodRange[i];
		auto j = i;

		while (j < goodRange.size())
		{
			if (goodRange[j + 1] - lastNum == 1) {
				lastNum = goodRange[j + 1];
				j++;
			}
			else {
				break;
			}
		}

		TimeInterval interval;

		interval.start = (double)i * timePerBucket;
		interval.end = (double)(j + durationLength) * timePerBucket;

		speechInterval.push_back(interval);

		i = j;
	}

	cout << speechInterval.size() << endl;

	// delete intervals that are too short
	for (auto iter = speechInterval.begin(); iter != speechInterval.end();) {
		auto duration = iter->end - iter->start;
		if (duration < 0.6) {
			iter = speechInterval.erase(iter);
		}
		else {
			++iter;
		}
	}

	cout << speechInterval.size() << endl;

	fstream outfile{ R"(C:\Users\xziyu\OneDrive\Thesis\Sample\sample2-subtitle-auto.txt)", ios::out };

	for (const auto &interval : speechInterval) {
		outfile << fixed << setprecision(5)
			<< interval.start << " " 
			<< fixed << setprecision(5)
			<< interval.end << "\n";
	}

	system("pause");


    return 0;
}

