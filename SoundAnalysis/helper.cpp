#pragma once

#include "helper.h"

::FLAC__StreamDecoderWriteStatus FLACDecoder::write_callback(const::FLAC__Frame * frame, const FLAC__int32 * const buffer[])
{
	auto denom = (float)numeric_limits<int16_t>::max();
	for (auto i = 0; i < (int)frame->header.blocksize; ++i) {
		this->audioData.push_back((float)(buffer[0][i])/denom);
	}
	return FLAC__STREAM_DECODER_WRITE_STATUS_CONTINUE;
}

void FLACDecoder::metadata_callback(const::FLAC__StreamMetadata * metadata)
{
	if (metadata->type == FLAC__METADATA_TYPE_STREAMINFO) {
		cout << "sample rate: " << metadata->data.stream_info.sample_rate << "\n";
		cout << "bits per channel: " << metadata->data.stream_info.bits_per_sample << "\n";
		cout << "number of samples: " << metadata->data.stream_info.total_samples << "\n";
		cout << "number of channels: " << metadata->data.stream_info.channels << "\n";

		Assert(metadata->data.stream_info.channels == 1, "only mono audio supported");
		Assert(metadata->data.stream_info.bits_per_sample == 16, "only 16-bit audio supported");

		totalSamples = (int)metadata->data.stream_info.total_samples;
		sampleRate = metadata->data.stream_info.sample_rate;
		
		this->audioData.clear();
		this->audioData.reserve(totalSamples);
	}
}

void FLACDecoder::error_callback(::FLAC__StreamDecoderErrorStatus status)
{
	cerr << "FLAC Error: " << FLAC__StreamDecoderErrorStatusString[status] << "\n";
	throw runtime_error{ "FLAC ERROR" };
}

AudioData ReadFLACFile(const string & filename)
{
	FLACDecoder decoder;
	auto initStatus = decoder.init(filename);
	if (initStatus != FLAC__STREAM_DECODER_INIT_STATUS_OK) {
		cerr << "libFLAC init failed: "<<FLAC__StreamDecoderInitStatusString[initStatus] << "\n";
		throw runtime_error{ "FLAC ERROR" };
	}

	bool ok = decoder.process_until_end_of_stream();
	if (!ok) {
		cout << "decoding failed : " << decoder.get_state().resolved_as_cstring(decoder) << "\n";
		throw runtime_error{ "FLAC ERROR" };
	}

	AudioData data;
	data.sampleRate = decoder.sampleRate;
	data.audioData = move(decoder.audioData);

	return data;
}
